var app = angular.module('wst', ['ngRoute']);
app.factory("Observe", ['$http', function($http) {
	var Observe = function()
	{
        var self = this;

        this.observers = [];
        this.watch = function(callback){
            self.observers.push(callback);
        };
        this.callObservers = function(object){
            angular.forEach(self.observers, function(callback){
                callback(object);
            });
        };

	};

	return (Observe);
}]);

app.factory("ApiFunctions", ['$http', function($http) {
	var ApiFunctions = function()
	{
        var self = this;

        this.defaultErrorCallback   = function(r,status)
        {
			modal.error('Status: '+status, r);
            console.log('Error: '+status, r);
			$('#btn-start-wave').prop('disabled',false);
        }

        this.apiGet = function(resource, funSuccess, headers, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;

			var req = {
				method: 'GET',
				url: 	resource+(token?'?token='+token:''),
				headers: headers
			}


			$http(req)
				.success(funSuccess)
                .error(funError);
        }

		this.apiPost = function(resource, data, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.post(resource+(token?'?token='+token:''), data)
                .success(funSuccess)
                .error(funError);
        }

		this.apiPut = function(resource, data, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.put(resource+(token?'?token='+token:''), data)
                .success(funSuccess)
                .error(funError);
        }

		this.apiDelete = function(resource, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.delete(resource+(token?'?token='+token:''))
                .success(funSuccess)
                .error(funError);
        }


	};

	return (ApiFunctions);
}]);


var time =
{
	// Lo que hay que sumarle a la hora local para estar a la hora del server
	offset		: 0,

	now			: function()
	{
		return this.nowClient() + this.offset;
	},

	nowClient	: function()
	{
		return Math.round(jQuery.now()/1000)
	},

	// Establece la hora del server
	setServer	: function(serverTime)
	{
		this.offset = serverTime - this.nowClient();
	},


	serverToClient : function(serverTime)
	{
		return serverTime - this.offset;
	},

	clientToServer : function(clientTime)
	{
		return clientTime + this.offset;
	},

	unixToString        : function (unix)
	{
		// Moment nos dará nuestra hora local, y queremos la UTC, osea que la falseamos
		unix = parseInt(unix) + new Date().getTimezoneOffset()*60;

		var day = moment.unix(unix);
		return day.format("YYYY-MM-DD HH:mm");
	},

	timeUntilUnix       : function (unix)
	{
		var string = countdown( unix*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.MONTHS | countdown.YEARS, 1 ).toString();
		if(now() - unix > 0)
			string += ' ago';
		else
			string = 'in ' + string;

		return string;
	},

	stringToUnix		: function(string)
	{
		var d = new Date(string);
		var unix = Math.floor(d.getTime()/1000);

		unix -= new Date().getTimezoneOffset()*60;

		return unix;

	}
}



$.getTime = function(zone, success) {
    var url = 'http://json-time.appspot.com/time.json?tz='
            + zone + '&callback=?';
    $.getJSON(url, function(o){
        success && success(new Date(o.datetime), o);
    });
};


var token = $.jStorage.get('token', false);


var modal =
	{
		error		: function(title, body)
		{
			if(typeof body != 'string')
				body = JSON.stringify(body);
			$('#modal_errors .modal-title').html(title);
			$('#modal_errors .modal-body').html(body);
			$('#modal_errors').modal();
		}
	}

// Reordena un Array poniendo como índice los IDs de sus elementos
function byId (array)
{
	var result = {};
	for(var i=0;i<array.length;i++)
	{
		result[array[i].id] = array[i];
	}
	return result;
}

function unixToUntilForHeats (unix)
{
	sign = '';
	elapsed = time.now() - unix;
	if(elapsed < 0)
	{
		elapsed = -elapsed;
		sign = '-';
	}

	hours = Math.floor(elapsed/3600);
	elapsed = elapsed - hours*3600;
	minutes = Math.floor(elapsed/60);
	elapsed = elapsed - minutes*60;
	seconds = elapsed;

	if(hours)
		hours = hours + 'h ';
	else
		hours = '';
	if(minutes)
		minutes = minutes + 'm ';
	else
		minutes = '';


	return sign + hours + minutes + seconds + 's';
}

function unixToUntil (unix)
{
	unix = time.serverToClient(unix);
	var string = countdown( unix*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.MONTHS | countdown.YEARS, 1 ).toString();
	if(string)
	{
		if(now() - unix > 0)
			string += ' ago';
		else
			string = 'in ' + string;
	}else{
		string = 'Null';
	}
	return string;
}
