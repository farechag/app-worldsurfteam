app.factory("Surfer", ['$http', '$rootScope', 'Observe', 'ApiFunctions', function($http, $rootScope, Observe, ApiFunctions) {
	var Surfer = function()
	{
		var self = this;
		var sc = $rootScope;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.all       		= false;


		// Propiedades



		// Métodos

			this.loadAll     = function()
			{
				self.apiGet('/api/v1/surfer', function(r)
				{
					self.all = byId(r);
				});
				return self;
			}

			this.get 		 = function(id)
			{
				return self.all[id];

			}

			this.add		= function(data)
			{
				if(data.wsl == undefined)
					data.wsl = false;
				self.apiPost('/api/v1/surfer', data, function(r)
				{
					sc.newAllSurfers = {};
					self.loadAll()
				});
			}

			this.del		= function(surferId)
			{
				self.apiDelete('/api/v1/surfer/'+surferId, function(r)
				{
					self.loadAll()
				});
			}

			this.update		= function(surferId, data)
			{
				self.apiPut('/api/v1/surfer/'+surferId, data, function(r)
				{
					self.loadAll()
				});
			}


		// Privados








	};

	return (Surfer);
}]);
