app.factory("User", ['$http', 'Observe', 'ApiFunctions', function($http, Observe, ApiFunctions) {
	var User = function()
	{
		var self = this;
        angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


        // Variables


            this.data       = undefined,



        // Propiedades

            // Bool : Is logued?
            this.logued    = function()
            {
                return self.data ? true : false;
            }

			// Bool : Is admin?
			this.admin		= function()
			{
				return self.data ? self.data.is_admin : false;
			}


        // Métodos

            this.check      = function()
            {
				if(token)
	                self.apiGet('/api/v1/account/session',function(r)
	                {
	                    self.data = r;
						if(!self.data.is_admin)
							alert('Necesitas ser admin.');
	                    self.callObservers(self);
	                });
            }

            this.login = function(data)
            {
                self.apiPut('/api/v1/account/session', data, function(r)
                {
                    self.data   = r.user;
					if(!self.data.is_admin)
						alert('Necesitas ser admin.');
                    token       = r.token;
                    $.jStorage.set('token', r.token, {TTL:13*24*60*60*1000});
                    self.callObservers(self);
                });
            }

            this.logout     = function()
            {
                token           = false;
                self.data       = false;
                $.jStorage.deleteKey('token');
                self.callObservers();
            }









    };
     
    // Return a reference to the function
    return (User);
}]);
