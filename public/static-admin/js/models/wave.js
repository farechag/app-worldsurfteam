app.factory("Wave", ['$http', '$rootScope', 'Observe', 'ApiFunctions', function($http, $rootScope, Observe, ApiFunctions) {
	var Wave = function()
	{
		var self = this;
		var sc	 = $rootScope;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.all			= [];

			this.lastEtag	= '';


		// Propiedades



		// Métodos

			this.load		= function()
			{
				if(sc.heat.id)
					self.apiGet('/api/v1/heat/'+sc.heat.id+'/waves',
					function(r, status, headers)
					{
						var newETag = false;
						if(headers().etag)
							newETag = headers().etag;

						if(!newETag || newETag != self.lastEtag)
						{
							self.all = r;
							self.lastEtag = newETag;
						}
					},
					{'If-None-Match' : self.lastEtag},
					function(r, status)
					{
						if(status == 304){ // NOT MODIFIED
							// nada
						}else{
							self.defaultErrorCallback(r,status);
						}
					});
			}



			this.new		= function(surferId)
			{
				$('#btn-start-wave').prop('disabled',true);
				self.apiPost('/api/v1/wave/', {heat_id:sc.heat.id,surfer_id:surferId},function(r)
				{
					self.load();
					sc.surfer_selected = false;
					$('#btn-start-wave').prop('disabled',false);
				});
			}


			this.del		= function(waveId)
			{
				self.apiDelete('/api/v1/wave/'+waveId, function(r)
				{
					self.load();
				});
			}

			this.finish		= function(waveId)
			{
				self.apiPut('/api/v1/wave/'+waveId+'/finish',{}, function(r)
				{
					self.load();
				});
			}



		// Privados


			this.refresh		= function()
			{
				if(self.data)
					self.participations = self.data.participations;
				else{
					self.participations = [];
				}


				// var heatId = self.currentId;
				// if(!heatId || self.all[eventId] == undefined)
				// {
				// 	self.current 		= false;
				// 	self.currentSurfers	= [];
				// }else{
				// 	self.current = self.all[eventId];
				// }
				//
				// var roundId = self.currentRoundId;
				// if(!eventId || !roundId || self.currentRounds[roundId] == undefined)
				// {
				// 	self.currentRound	= false;
				// 	self.currentHeats	= [];
				// }else{
				// 	self.currentRound	= self.currentRounds[roundId];
				// 	self.currentHeats	= self.currentRound.heats;
				// }
			}






	};

	return (Wave);
}]);
