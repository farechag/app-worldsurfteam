app.factory("Event", ['$http', 'Observe', 'ApiFunctions', function($http, Observe, ApiFunctions) {
	var Event = function()
	{
		var self = this;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.all       		= false;

			// Current
			this.currentId		= false;
			this.current		= false;
			this.currentSurfers	= [];
			this.currentRounds	= [];
			this.currentRoundId = false;
			this.currentHeats	= [];

		// Propiedades



		// Métodos

			this.loadAll     = function()
			{
				self.apiGet('/api/v1/event/', function(r)
				{
					self.all = byId(r);
					self.refreshCurrent();
				});
				return self;
			}



			// Pone como Evento actual el evento que le pasemos
			this.set		= function(eventId)
			{
				self.currentId = eventId;
				self.loadSurfers();
				self.loadHeats();
			}

			this.setRound	= function(roundNumber)
			{
				self.currentRoundId = roundNumber;
			}

			this.loadSurfers	= function()
			{
				if(self.currentId)
				self.apiGet('/api/v1/event/'+self.currentId+'/surfers', function(r)
				{
					self.currentSurfers = r;
					self.refreshCurrent();
				});
			}

			this.addSurfer	= function(surferId)
			{
				self.apiPut('/api/v1/event/'+self.currentId+'/surfer/'+surferId, [], function(r)
				{
					self.loadSurfers();
				});
			}

			this.delSurfer	= function(surferId)
			{
				self.apiDelete('/api/v1/event/'+self.currentId+'/surfer/'+surferId, function(r)
				{
					self.loadSurfers();
				});
			}

			this.loadHeats	= function()
			{
				if(self.currentId)
				self.apiGet('/api/v1/event/'+self.currentId+'/heats', function(r)
				{
					self.currentRounds = r;
					self.refreshCurrent();
				});
			}

			this.delHeat	= function(heatId)
			{
				self.apiDelete('/api/v1/heat/'+heatId, function(r)
				{
					self.loadHeats();
				});
			}


			this.new		= function(data)
			{
				var data2 = jQuery.extend(true, {}, data);
				data2.start_date = time.stringToUnix(data.start_date);
				data2.finish_date = time.stringToUnix(data.finish_date);
				self.apiPost('/api/v1/event', data2, function(r)
				{
					self.loadAll()
				});
			}

			this.createRounds	= function(number)
			{
				self.apiPut('/api/v1/event/'+self.currentId+'/round',{rounds_count:number}, function(r)
				{
					self.loadHeats();
				});

			}



		// Privados


			this.refreshCurrent		= function()
			{
				var eventId = self.currentId;
				if(!eventId || self.all[eventId] == undefined)
				{
					self.current 		= false;
					self.currentSurfers	= [];
				}else{
					self.current = self.all[eventId];
				}

				var roundId = self.currentRoundId;
				if(!eventId || !roundId || self.currentRounds[roundId] == undefined)
				{
					self.currentRound	= false;
					self.currentHeats	= [];
				}else{
					self.currentRound	= self.currentRounds[roundId];
					self.currentHeats	= self.currentRound.heats;
				}
			}






	};

	return (Event);
}]);
