app.config(function($routeProvider) {
		$routeProvider

		// route for the home page
		.when('/', {
			templateUrl : 'static-admin/partials/temp1.html',
			controller  : function($scope, $rootScope, $routeParams)
			{
				$rootScope.root = '/';
				$rootScope.event.set(false);
				$rootScope.event.setRound(false);
				$rootScope.heat.set(false);
			}
		})

		// route for the about page
		.when('/event/:event_id', {
			templateUrl : 'static-admin/partials/temp1.html',
			controller  : function($scope, $rootScope, $routeParams)
			{
				$rootScope.root = 'event';
				$rootScope.event.set($routeParams.event_id);
				$rootScope.event.setRound(false);
				$rootScope.heat.set(false);
			}
		})

		// route for the about page
		.when('/event/:event_id/round/:round_id', {
			templateUrl : 'static-admin/partials/temp1.html',
			controller  : function($scope, $rootScope, $routeParams)
			{
				$rootScope.root = 'round';
				$rootScope.event.set($routeParams.event_id);
				$rootScope.event.setRound($routeParams.round_id);
				$rootScope.heat.set(false);
			}
		})

		// route for the about page
		.when('/event/:event_id/round/:round_id/heat/:heat_id', {
			templateUrl : 'static-admin/partials/temp1.html',
			controller  : function($scope, $rootScope, $routeParams)
			{
				$rootScope.root = 'heat';
				$rootScope.event.set($routeParams.event_id);
				$rootScope.event.setRound($routeParams.round_id);
				$rootScope.heat.set($routeParams.heat_id);
			}
		});
});
