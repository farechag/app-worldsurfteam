app.controller('mainCtrl', ['$http', '$scope', '$rootScope', '$interval', '$timeout', 'User', 'Event', 'Surfer', 'Heat', 'Wave',
function ($http, $scope, $rootScope, $interval, $timeout, User, Event, Surfer, Heat, Wave)
{
    var vm=this;
    var sc=$rootScope;

	// Models

        sc.user 	= new User();
        sc.event	= new Event();
        sc.surfer   = new Surfer();
        sc.heat     = new Heat();
        sc.wave     = new Wave();

        sc.countries    = {};
        sc.eventTypes   = {};
        sc.eventSponsors= {};



    // Métodos base

        sc.log       = function(x)
        {
            console.log(x);
        }

        sc.alert    = function(x)
        {
            alert(x);
        }

    	this.init	= function()
    	{
            $http.get('/api/v1/server/time')
            .success(function(timeServer)
            {
                time.setServer(timeServer);
            });

            $http.get('/api/v1/country')
            .success(function(countries)
            {
                sc.countries = byId(countries);
            });

            $http.get('/api/v1/event/get_types')
            .success(function(types)
            {
                sc.eventTypes = byId(types);
            });

            $http.get('/api/v1/event/get_sponsors')
            .success(function(sponsors)
            {
                sc.eventSponsors = byId(sponsors);
            });


            // Comprobamos el usuario
            sc.user.check();
            sc.$watch('user',function()
            {
                // Mostramos body
                $('body').show();
            });

            // Cargamos todos los eventos
            sc.event.loadAll();

            // Cargamos todos los surfers
            sc.surfer.loadAll();

    	}


    // Watches




    // Métodos

    // Los heats los actualizamos para siempre
    $interval(updateUnixTimesForHeats, 1000);
        function updateUnixTimesForHeats ()
        {
            $('.unix-to-until-for-heats').each(function()
            {
                var unix    = $(this).attr('unix');
                if(Math.abs(now() - unix) > 0 && Math.abs(now() - unix) || $(this).html().length == 0)
                {
                    var string = unixToUntilForHeats(unix);
                    $(this).html(string)
                }
            });
        }


    $interval(unixTimesMax10min,     1*1000);
    $interval(unixTimesMax1hour,    60*1000);
    $interval(unixTimesInfinite,    10*60*1000);
        function unixTimesMax10min ()
        {
            updateUnixTimes(10);
        }
        function unixTimesMax1hour ()
        {
            updateUnixTimes(60);
        }
        function unixTimesInfinite ()
        {
            updateUnixTimes();
        }
        function updateUnixTimes (maxAge)
        {
            $('.unix-to-until').each(function()
            {
                var unix    = $(this).attr('unix');
                var ageCondition = true;
                if(maxAge != undefined)
                    ageCondition = Math.abs(now() - unix) < maxAge*60;

                if((Math.abs(now() - unix) > 0 && ageCondition) || $(this).html().length == 0)
                {
                    var string = unixToUntil(unix);
                    $(this).html(string)
                }
            });
        }





    $interval(updateWaves, 3000);
    function updateWaves ()
    {
        sc.wave.load();
    }

    $interval('updateHeat', 3000);
    function updateHeat ()
    {
        sc.heat.load();
    }







}]);
