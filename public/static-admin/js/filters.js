app

	.filter('object2Array', function() {
		return function(input) {
		var out = [];
		for(i in input){
			out.push(input[i]);
		}
		return out;
		}
	})

	.filter('active', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].active)
					out.push(input[i]);
			}
			return out;
		}
	})
	.filter('inactive', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(!input[i].active)
					out.push(input[i]);
			}
			return out;
		}
	})


	// Devuelve todos los elementos que estén en list (usa la ID)
	.filter('in', function(list) {
		return function(input) {
			var out = [];
			for(i in input){
				var item_in = input[i];
				for(j in list)
				{
					var item_test = list[j];
					if(item_in.id == item_test.id)
					{
						out.push(item_in);
						break;
					}
				}
			}
			return out;
		}
	})

	// Devuelve todos los elementos que NO estén en list (usa la ID)
	.filter('notIn', function() {
		return function(input, list) {
			var out = [];
			for(i in input){
				var item_in = input[i];
				var found = false;
				for(j in list)
				{
					var id = false;
					if(typeof list[j] == 'object')
						id = list[j].id;
					else
						id = list[j];
					if(item_in.id == id)
					{
						found = true;
						break;
					}
				}
				if(!found)
					out.push(item_in);
			}
			return out;
		}
	})

	.filter('onlySurfersWSL', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].wsl)
					out.push(input[i]);
			}
			return out;
		}
	})

	.filter('onlySurfersNotWSL', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(!input[i].wsl)
					out.push(input[i]);
			}
			return out;
		}
	})

	.filter('length', function() {
		return function(input) {
			if(input == undefined)
				return [];
			return Object.keys(input).length;
		}
	})

	.filter('unixToString', function() {
		return function(input) {
			return time.unixToString(input);
		}
	})

	.filter('onlyNotActive', function() {
		return function(input) {
		var out = [];
		for(i in input){
			if(!input[i].active)
				out.push(input[i]);
		}
		return out;
		}
	})
	.filter('onlyActive', function() {
		return function(input) {
		var out = [];
		for(i in input){
			if(input[i].active)
				out.push(input[i]);
		}
		return out;
		}
	})
