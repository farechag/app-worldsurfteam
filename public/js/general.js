
    /*****************************************************************************************************
     ************************************	Conexión con Servidor    *************************************
     *****************************************************************************************************/

    // Manda una orden (acompañada o no de datos) al servidor. Cuando el servidor responda se llama a la
    // la función FUNC, pasandole los datos que el servidor ha revuelto.

    function exec(action, datos, func, async){
        return api.send(action, datos, func);
    }
    // Función para traducir códigos de error a las respectivas descripciones del error, en el idioma del usuario

    function get_error_description(error)
    {
        return error;
        // El servidor puede devolver dos cosas en el campo del error:
        // Puede devolver un string directamente. En ese caso esta función no hará nada.
        // Puede devolver un código de error. En ese caso comprueba si tenemos una descripción de ese error
        // en el idioma del usuario, y si es así devuelve esa descripción. En caso contrario devuelve lo que se le pasó.

        if(typeof lang.ajax_errors[error] != 'undefined')
            return lang.ajax_errors[error];
        else
            return error;
    }


    function now()
    {
        return Math.round(jQuery.now()/1000)
    }
    /*****************************************************************************************************
     ************************************	Útiles		     *********************************************
     *****************************************************************************************************/

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
  }

  function log(x)
  {
    console.log(x);  
  }

    function get_time(){
        return new Date().getTime();
    }

    // Default input
    function setDefault(input, default_value)
    {
        if(typeof input === "undefined" || input == 'undefined')
            return default_value;
        return input;
    }

    // Para que Date.now valga en navegadores antiguos ( IE <= 8 )
    if (!Date.now) {
        Date.now = function() { return new Date().getTime(); };
    }

    // Elimina una KEY de un array

    /*
    Object.prototype.removeItem = function (key) {
       if (!this.hasOwnProperty(key))
          return;
       if (isNaN(parseInt(key)) || !(this instanceof Array))
          delete this[key];
       else
          this.splice(key, 1);
    };


    /*****************************************************************************************************
     ************************************	Seguridad	     *********************************************
     *****************************************************************************************************/

    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };
    var entityMap2 = [
        "&-&amp;",
        "<-&lt;",
        ">-&gt;",
        '"-&quot;',
        "'-&#39;",
        "/-&#x2F;"
    ];
    function escapeHtml(string) {
        return String(string).replace(/[&<>"'\/]/g, function (s) {
            return entityMap[s];
        });
    }
    function unescapeHtml(string) {
        for(var i = 0; i< entityMap2.length; i++){
            var v = entityMap2[i].split('-');
            string = string.split(v[1]).join(v[0]);
        }
        return string;
    }

    function escapeJs(string) {
        if(typeof string == 'undefined')
            return '';
        string = setDefault(string, '');
        return string.replace(/'/g,"\\'").replace(/"/g,'\\"');
    }
    function escapeRegExp(str) {
          return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    function loadTemplate(file){

        var $template_div = $('#volcados #template_' + file);
        if($template_div.length){
            // Lo cargamos directo del contenedor
            result = $template_div.html();
        }else{
            // Lo cargamos por ajax
            $.ajax({
                url: '/templates/'+file+'.html',
                type: 'get',
                dataType: 'html',
                async: false,
                cache:false,
                success: function(data) {
                    result = data;
                }
        });
        }
     return result;

    }
    function renderTemplate(file, data){
          t = doT.template(loadTemplate(file));
          $("#content").html(t(data));
    }








(function( $ ){

    // Pasa los hijos de un elemento (hijos válidos: INPUT, SELECT y TEXTAREA) a un OBJECT
    $.fn.objetize=function() {
        var result = new Object();
        $(this).find('input, select, textarea').each(function(i){
            if (!$(this).hasClass("no_send")){

                if($(this).prop('type') == 'radio'){
                    if($(this).prop('checked'))
                        result[$(this).attr('name')] = $(this).val();
                }else if($(this).prop('type') == 'checkbox'){
                        result[$(this).attr('name')] = $(this).prop('checked');
                }else if($(this).prop('type') == 'file'){
                    //var file = $(this)[0].files
                    //result[$(this).attr('name').replace('[]','')] = file;
                }else if($(this).prop('type') == 'button'){

                }else{
                    result[$(this).attr('name')] = $(this).val();
                }

            }
        });
        return result;
    };

    // Comprueba uno por uno todos los INPUTS, viendo si contienen sintaxis válida
    $.fn.checkForm = function(){

        var valid = true;

        this.find('.help-inline').hide().html('');
        this.find('.control-group').removeClass('error');

        this.find('input[type="text"], input[type="pass"], input[type="password"], textarea')
        .each(function(){

            // Vars
            var $control_group = $(this).closest('.control-group'),
                $help = $control_group.find('span.help-inline');

            // Comprobamos
            if($(this).check() == '')
                return true;	// Continue

            // Informamos
            $help.html(check_last_error).show();
            $control_group.addClass('error');

            // Salimos
            valid = false;
            return false;		// Break
        });

        return valid;
    };

    // Comprueba un input concreto, según el criterio establecido en data-check
    $.fn.check = function()
    {

        /**
         *  jQuery Check
         *
         *  Comprueba un INPUT en función del criterio introducido en su propiedad data-check.
         * 	Si esta propiedad no está almacenada, se considera al input correcto.
         *  Devuelve un String vacío si todo ha ido bien y un mensaje de error en caso contrario.
         *
         * 	Un ejemplo de data-check:	"5:17:^[a-zA-Z]*$"
         *  Donde 5 es long. mínima, 17 máxima, y un regexp.
         *
         *  Si data-check no tiene ':' se considera que es un check complejo, que se busca en
         *  las que tiene la función guardada. Si no figura, se considera erroneo.
         *
         */

        check_last_error = '';

        // Comprobemos que nos pasan un objeto y solo uno
        if ( this.length != 1)	return '';

        // Si no tiene datos de check, vale todo
        if (typeof this.data('check') == 'undefined')	return '';
        var value		= this.val(),
            criterio	= this.data('check');

        c = Core.criteria[criterio];
        arr = c.split(':'),
        result = arr.splice(0,2);
        result.push(arr.join(':'));
        min = result[0].length==0 ? 0:parseInt(result[0]);
        max = result[1].length==0 ? 0:parseInt(result[1]);
        return this.val().length>=min && this.val().length<=max && this.val().match(result[2])!=null && this.val().match(result[2]).length>0;

    };

})( jQuery );
