app

	.filter('object2Array', function() {
		return function(input) {
		var out = [];
		for(i in input){
			out.push(input[i]);
		}
		return out;
		}
	})

	.filter('active', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].active)
					out.push(input[i]);
			}
			return out;
		}
	})

	.filter('heatElapsedPer', function() {
		return function(input) {

			var start = input.start_date;
			var finish = input.finish_date;

			var duration = finish - start;
			var elapsed  = time.now() - start;

			if(duration < 0 || elapsed < 0)
				return 0;
			if(elapsed > duration)
				return 100;

			return Math.floor(100*elapsed/duration);
		}
	})

	.filter('delete', function() {
		return function(input, str1) {
			if(typeof input != 'string')
				return '';

			out = input.split(str1).join('');
			return out;
		}
	})

	.filter('iniciales', function() {
		return function(input, str1) {
			out = input.replace(/[^A-Z]/g, '');
			return out;
		}
	})

	.filter('getSurferIndex', function() {
		return function(input, surferId) {
			for(i in input){
				if(input[i].surfer_id == surferId)
					return i;
			}
			return -1;
		}
	})

	.filter('inactive', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(!input[i].active)
					out.push(input[i]);
			}
			return out;
		}
	})

	.filter('started', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].start_date < time.now() && input[i].finish_date > time.now())
					out.push(input[i]);
			}
			return out;
		}
	})
	.filter('not_started', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].start_date == null || input[i].start_date > time.now())
					out.push(input[i]);
			}
			return out;
		}
	})
	.filter('started_but_not_finished', function() {
		return function(input) {
			var out = [];
			for(i in input){

				// Not started
				if(input[i].start_date == null || input[i].start_date > time.now())
					continue;

				// Finished
				if(input[i].finish_date != null && input[i].finish_date < time.now())
					continue;

				out.push(input[i]);
			}
			return out;
		}
	})

	.filter('log', function() {
		return function(input) {
			console.log(input);
			return input;
		}
	})

	.filter('validVote', function() {
		return function(input) {

			if(!jQuery.isNumeric(input))
				return false;
			if(input < 0 || input > 10)
				return false;

			return true;
		}
	})

	.filter('activeAndRecentlyVoted', function() {
		return function(input) {
			var out = [];
			for(i in input){

				recentlyVoted = false;
				if(input[i].vote && (now() - input[i].vote_date) < 7)
					recentlyVoted = true;

				if(input[i].active && (!input[i].vote || recentlyVoted))
				{
					out.push(input[i]);
				}else{

				}
			}
			return out;
		}
	})

	.filter('recentlyVoted', function() {
		return function(input) {
			var out = [];
			for(i in input){

				recentlyVoted = false;
				if(input[i].vote && (now() - input[i].vote_date) < 7)
					recentlyVoted = true;

				if(recentlyVoted)
				{
					out.push(input[i]);
				}else{

				}
			}
			return out;
		}
	})

	.filter('not_voted', function() {
		return function(input) {
			var out = [];
			for(i in input){
				if(input[i].vote !== false)
					out.push(input[i]);
			}
			return out;
		}
	})

	// rellena con ceros
	.filter('pad', function() {
		return function(input, min_length) {
			while(input.toString().length < min_length)
				input = '0' + input;

			return input;
		}
	})

	// // No funciona
	// .filter('updateEverySecond', function() {
	// 	return function(input) {
	// 		input = input + '//' + time.now();
	//
	// 		return input;
	// 	}
	// })


	.filter('time', function() {
		return function(input, option) {
			if(input == undefined)
				return;
			if(typeof input == 'string')
			{
				input = input.split('//');
				input = input[0];
			}
			switch(option)
			{
				case 'server2client':
					return time.serverToClient(input);
				case 'until':
					if(input - time.now() < 10)
						return 'In a few seconds!'
					var string = countdown( input*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.MONTHS | countdown.YEARS, 2 ).toString();
					if(string)
					{
						if(time.now() - input > 0)
							string += ' ago';
						else
							string = 'in ' + string;
					}else{
						string = '';
					}
					return string;
				case 'until-secs':
					return input - time.now();
				case 'elapsed':

					var s = Math.abs(input - time.now());

					var m = Math.floor(s/60);
						s -= m*60;

					if(m < 10)
						m = '0' + m;
					if(s < 10)
						s = '0' + s;

					return m + ':' + s;
				case 'date-verbose':
					if(input == undefined)
						return;

					var day = moment.unix(input);


					return day.format("ddd MMMM D YYYY");
				case 'until-day-hours-min-sec':

					var s = Math.abs(input - time.now());

					var d = Math.floor(s/(24*3600));
						s -= d*(24*3600);
					var h = Math.floor(s/3600);
						s -= h*3600;
					var m = Math.floor(s/60);
						s -= m*60;

					if(h < 10)
						h = '0' + h;
					if(m < 10)
						m = '0' + m;
					if(s < 10)
						s = '0' + s;

					return d + ' : ' + h + ' : ' + m + ' : ' + s;
			}

			return '';
		}
	})

	// Formatea una fecha
	.filter('format', function() {
		return function(input, format) {
			var day = moment.unix(input);
			return day.format(format);
		}
	})

	.filter('length', function() {
		return function(input) {
			if(input == undefined)
				return [];
			return Object.keys(input).length;
		}
	});
