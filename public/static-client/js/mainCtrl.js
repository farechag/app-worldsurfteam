app.controller('mainCtrl', ['$http', '$window', '$scope', '$rootScope', '$interval', '$timeout', 'User', 'Event', 'Heat', 'Wave', 'Surfer', 'ApiFunctions',
function ($http, $window, $scope, $rootScope, $interval, $timeout, User, Event, Heat, Wave, Surfer, ApiFunctions)
{
    var vm=this;
    var sc=$rootScope;

    var api = new ApiFunctions();

	// Models

        sc.user 	= new User();
        sc.event	= new Event();
        sc.heat	    = new Heat();
        sc.wave	    = new Wave();
        sc.surfer   = new Surfer();
        // sc.country  = {};
        //
        // sc.countries = {};

        sc.welcomeLoading = true;

        $scope.next_event = 5;
        sc.bg_zoomed = true;


        sc.eventId  = false;


    // Métodos base

        sc.go       = function(url)
        {
            location.href = url;
        }

        sc.goBack   = function()
        {
            $window.history.back();
        }

        sc.alert    = function(x)
        {
            alert(x);
        }

        sc.log    = function(x)
        {
            log(x);
            return x;
        }

        sc.increment = function(x)
        {
            x = x+1;
        }

        sc.now         = function()
        {
            return time.now();
        }
        sc.addNow         = function()
        {
            return '//' + Math.floor(jQuery.now()/300);
        }

        sc.randomColor = function(index)
        {
        	var colors = [ '#DA4453', '#4A89DC', '#8CC150',
                            '#5D9CEC', '#4FC1E9', '#48CFAD', '#A0D468', '#FFCE54', '#FC6E51',
                            '#ED5565', '#89C4F4', '#EC87C0'];

        	index = index % colors.length;

        	return colors[index];
        }

        sc.surfersColors = function(index)
        {
        	var colors = [ '#2b78e4', '#ff9900', '#009e0f', '#ec008c'];

        	index = index % colors.length;

        	return colors[index];
        }

        sc.surfersColorsRgba = function(index, alfa)
        {
        	var colors = [     'rgba(43, 120, 228,' + alfa +')',
                               'rgba(255, 153, 0,' + alfa +')',
                               'rgba(0, 158, 15,' + alfa +')',
                               'rgba(236, 0, 140,' + alfa +')'
                        ];

        	index = index % colors.length;

        	return colors[index];
        }

        sc.showMenu = function()
        {
            $('#menu_page').show();
            $('#menu_page .columna').animate({'width': '70%'}, 200);
            $('#menu_page .bg-closer').show();
        }
        sc.closeMenu = function()
        {

            $('#menu_page .columna').animate({'width': '0%'}, 200, function(){
                $('#menu_page').hide();
            });
            $('#menu_page .bg-closer').hide();
        }


    	this.init	= function()
    	{
            $http.get('/api/v1/server/time')
            .success(function(timeServer)
            {
                time.setServer(timeServer);
            });

            // Países
            // sc.countries = $.jStorage.get('countries', false);
            // if(!sc.countries)
            //     $http.get('/api/v1/country')
            //     .success(function(countries)
            //     {
            //         sc.countries = byId(countries);
            //         $.jStorage.set('countries', sc.countries);
            //     });



            // Comprobamos el usuario
            sc.user.check();

            // Cargamos todos los eventos
            sc.event.loadAll();

            // Cargamos todos los surfers
            sc.surfer.loadAll();

            // Esperamos 5 segundos para el welcome
            $timeout(function()
            {
                sc.welcomeLoading = false;
            }, server_env == 'local' ? 500 : 5000);


    	}


    // Slideshow

        sc.slideshowStarted = false;
        sc.slideshowIndex = 0;
        sc.slideshowPause = false;
        sc.$watch('root', function(newValue, oldValue)
        {
            root = newValue;
            if(!sc.slideshowStarted)
            {
                setInterval(function() {
                    if(root != '/' || sc.slideshowPause)
                        return;
                    var elements = $('.slideshow > li').length;
                    var actual = $('.slideshow > li:eq('+sc.slideshowIndex+')');
                    sc.slideshowIndex = (sc.slideshowIndex + 1) % elements;
                    var next   = $('.slideshow > li:eq('+sc.slideshowIndex+')');


                    actual.fadeOut(1000);
                    next.fadeIn(1000);
                },  6000);
                setTimeout(function() {
                    $(".slideshow .message").mouseenter(function(){
                        sc.slideshowPause = true;
                    }).mouseleave(function(){
                        sc.slideshowPause = false;
                    });
                },  5000);





                sc.slideshowStarted = true;
            }


        });



    // Métodos

        sc.forceUpdateUnix = function()
        {
            //updateUnixTimes();
        }

        // Para la ventana Events
        this.set_info_event = function(eventId)
        {
            api.apiGet('/api/v1/event/'+eventId+'/heats', function(r)
            {
                var heats = {};
                for(i in r)
                {
                    var round = r[i];
                    for(j in round.heats)
                    {
                        var heat = round.heats[j];
                        heats[heat.id] = heat;
                    }
                }

                vm.info_event_start = sc.event.all[eventId].start_date;
                vm.info_heats       = heats;

            });



        }



    // Los heats los actualizamos para siempre
    $interval(updateUnixTimesForHeats, 1000);
        function updateUnixTimesForHeats ()
        {
            if(sc.root == 'heat' || sc.root == 'event')
            $('.unix-to-until-for-heats').each(function()
            {
                var unix    = $(this).attr('unix');
                if(Math.abs(time.now() - unix) > 0 && Math.abs(time.now() - unix) || $(this).html().length == 0)
                {
                    var string = unixToUntilForHeats(unix, true);
                    $(this).html(string)
                }
            });
            $('.unix-to-until-for-info-row').each(function()
            {
                var unix    = $(this).attr('unix');
                if(Math.abs(time.now() - unix) > 0 && Math.abs(time.now() - unix) || $(this).html().length == 0)
                {
                    var string = unixToUntilForHeats(unix, false);

                    $(this).html(string)
                }
            });
        }

    // Los events los actualizamos para siempre
    $interval(updateUnixTimesForEvents, 1000);
        function updateUnixTimesForEvents ()
        {
            if(sc.root == '/')
            $('.unix-to-until-for-events').each(function()
            {
                var unix    = $(this).attr('unix');
                if(Math.abs(time.now() - unix) > 0 && Math.abs(time.now() - unix) || $(this).html().length == 0)
                {
                    var string = unixToUntilForEvents(unix);
                    $(this).html(string)
                }
            });
        }


    $interval(unixTimesMax10min,     1*1000);
    $interval(unixTimesMax1hour,    60*1000);
    $interval(unixTimesInfinite,    10*60*1000);
        function unixTimesMax10min ()
        {
            updateUnixTimes(10);
        }
        function unixTimesMax1hour ()
        {
            updateUnixTimes(60);
        }
        function unixTimesInfinite ()
        {
            updateUnixTimes();
        }
        function updateUnixTimes (maxAge)
        {
            $('.unix-to-until').each(function()
            {
                var unix    = $(this).attr('unix');
                var ageCondition = true;
                if(maxAge != undefined)
                    ageCondition = Math.abs(time.now() - unix) < maxAge*60;

                if((Math.abs(time.now() - unix) > 0 && ageCondition) || $(this).html().length == 0)
                {
                    var string = unixToUntil(unix);
                    $(this).html(string)
                }
            });
        }




    $interval(updateEvents, 10000);
    function updateEvents ()
    {
        if(sc.root == '/')
            sc.event.loadAll();
    }

    $interval(updateWaves, 3000);
    function updateWaves ()
    {
        if(sc.root == 'heat')
            sc.wave.load();
    }

    $interval(updateHeats, 3000);
    function updateHeats ()
    {
        if(sc.root == 'event')
            sc.event.loadHeatsIntervalFunction();
    }

    $interval(updateHeat, 3000);
    function updateHeat ()
    {
        if(sc.root == 'heat')
            sc.heat.load();
    }



    $interval(backgroundZoom, 58*1000);
    backgroundZoom();
    function backgroundZoom()
    {
        if(sc.bg_zoomed)
        {
            $('#wrapper-view .backstretch img').addClass('bg-zoom');
            $('#wrapper-view .backstretch img').removeClass('bg-zoom-reverse');
            sc.bg_zoomed = false;
        }else{
            $('#wrapper-view .backstretch img').addClass('bg-zoom-reverse');
            $('#wrapper-view .backstretch img').removeClass('bg-zoom');
            sc.bg_zoomed = true;
        }
    }


}]);
