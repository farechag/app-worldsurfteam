app.config(function($routeProvider) {
		$routeProvider

		// route for the home page
		.when('/', {
			templateUrl : 'static-client/pages/events.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				//$("#home_view").backstretch("http://mardegalicia.com/assets/upload/activity_images/original/1365676369OLFEMT.jpeg");
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = '/';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app/home'
				});
			}
		})

		// route for the home page
		.when('/news/:news_id', {
			templateUrl : 'static-client/pages/news.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'news';
				$rootScope.new_id = $routeParams.news_id;

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})

		// route for the about page
		.when('/event/:event_id', {
			templateUrl : 'static-client/pages/event.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				$rootScope.event.set($routeParams.event_id);
				$rootScope.heat.set(false);
				$rootScope.root = 'event';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})

		// route for the about page
		.when('/event/:event_id/heat/:heat_id', {
			templateUrl : 'static-client/pages/heat.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				$rootScope.event.set($routeParams.event_id);
				$rootScope.heat.set($routeParams.heat_id);
				$rootScope.root = 'heat';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})

		// route for the about page
		.when('/register', {
			templateUrl : 'static-client/pages/register.html',
			controller  : function($location, $scope, $rootScope, $routeParams, $http)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				if(!$rootScope.countries)
					$http.get('/api/v1/country')
		            .success(function(countries)
		            {
		                $rootScope.countries = byId(countries);
		            });
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'register';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})

		// route for the about page
		.when('/register/needed', {
			templateUrl : 'static-client/pages/register.html',
			controller  : function($location, $scope, $rootScope, $routeParams, $http)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.registerMessage = 'You need to be registered to vote. Only 10 seconds dude!';
				if(!$rootScope.countries)
					$http.get('/api/v1/country')
		            .success(function(countries)
		            {
		                $rootScope.countries = byId(countries);
		            });
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'register';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})

		// route for the about page
		.when('/login', {
			templateUrl : 'static-client/pages/login.html',
			controller  : function($location, $scope, $rootScope, $routeParams, $http)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.registerMessage = false;
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'login';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app'+$location.$$path
				});
			}
		})


		// route for the about page
		.when('/password_reset', {
			templateUrl : 'static-client/pages/password_reset_send.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'password_reset_send';

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app/password_reset_send'
				});
			}
		})

		// route for the about page
		.when('/password_reset/:code', {
			templateUrl : 'static-client/pages/password_reset.html',
			controller  : function($location, $scope, $rootScope, $routeParams)
			{
				$rootScope.welcomeLoading = false;

				$rootScope.closeMenu();
				$rootScope.event.set(false);
				$rootScope.heat.set(false);
				$rootScope.root = 'password_reset';
				$rootScope.password_reset_code = $routeParams.code;

				// Google Analytics
				ga('send', 'pageview', {
					'page': 'app/password_reset'
				});
			}
		});
});
