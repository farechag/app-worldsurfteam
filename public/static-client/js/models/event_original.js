app.factory("Event", ['$http', 'Observe', 'ApiFunctions', function($http, Observe, ApiFunctions) {
	var Event = function()
	{
		var self = this;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.all       		= false;

			// Current
			this.currentId		= false;
			this.current		= false;

		// Propiedades



		// Métodos

			this.loadAll     = function()
			{
				self.apiGet('/api/v1/event/', function(r)
				{
					self.all = byId(r);
					self.refreshCurrent();
				});
				return self;
			}

			// Carga las heats del evento actual
			// NO hace falta, está dentro de event.round.heats
			// this.loadHeats	= function()
			// {
			// 	if(!self.currentId)
			// 		return;
			// 	self.apiGet('/api/v1/event/'+this.currentId+'/heats', function(r)
			// 	{
			// 		self.currentHeats = byId(r);
			// 		self.refreshCurrent();
			// 	});
			// 	return self;
			// }


			// Pone como Evento actual el evento del que le heblemos
			this.set		= function(eventId)
			{
				self.currentId = eventId;
				self.refreshCurrent();
			}




		// Privados


			this.refreshCurrent		= function()
			{
				var eventId = self.currentId;
				if(!eventId || self.all[eventId] == undefined)
				{
					self.current = false;
				}else{
					self.current = self.all[eventId];
				}
			}






	};

	return (Event);
}]);
