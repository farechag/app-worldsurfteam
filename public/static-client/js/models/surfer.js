app.factory("Surfer", ['$http', 'Observe', 'ApiFunctions', function($http, Observe, ApiFunctions) {
	var Surfer = function()
	{
		var self = this;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.all       		= false;


		// Propiedades



		// Métodos

			this.loadAll     = function()
			{
				self.apiGet('/api/v1/surfer', function(r)
				{
					self.all = byId(r);
				});
				return self;
			}

			this.get 		 = function(id)
			{
				return self.all[id];
			}



		// Privados








	};

	return (Surfer);
}]);
