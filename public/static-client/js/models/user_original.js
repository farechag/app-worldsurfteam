app.factory("User", ['$http', 'Observe', 'ApiFunctions', function($http, Observe, ApiFunctions) {
	var User = function()
	{
		var self = this;
        angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


        // Variables


            this.data       = false,



        // Propiedades

            // Bool : Is logued?
            this.logued    = function()
            {
                return self.data ? true : false;
            }


        // Métodos

            this.check      = function()
            {
                self.apiGet('/api/v1/account/',function(r)
                {
                    self.data = r;
                    self.callObservers(self);
                });
            }

            this.login = function(data)
            {
                self.apiPost('/api/v1/account/login',function(r)
                {
                    self.data   = r.user;
                    token       = r.token;
                    $.jStorage.set('token', r.token, {TTL:13*24*60*60*1000});
                    self.callObservers(self);
                });
            }


            this.logout     = function()
            {
                token           = false;
                self.data       = false;
                //$.jStorage.deleteKey('token');
                self.callObservers();
            }









    };
     
    // Return a reference to the function
    return (User);
}]);
