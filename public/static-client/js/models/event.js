app.factory("Event", ['$http', '$rootScope', 'Observe', 'ApiFunctions', function($http, $rootScope, Observe, ApiFunctions) {
	var Event = function()
	{
		var self = this;
		var sc = $rootScope;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.loading		= true;
			this.all       		= false;
			this.lastEtag		= '';
			this.lastHeatsEtag	= '';


			// Current
			this.currentId		= false;
			this.current		= false;
			this.currentSurfers	= [];
			this.currentRounds	= [];
			this.currentRoundId = false;
			this.currentHeats	= [];


		// Propiedades



		// Métodos

			this.loadAll     = function()
			{
				self.apiGet('/api/v1/event',
				function(r, status, headers)
				{
					var newETag = false;
					if(headers().etag)
						newETag = headers().etag;

					if(!newETag || newETag != self.lastEtag)
					{
						//***************

						self.all = byId(r);
						self.refreshCurrent();

						//***************
						// Actualizamos etag
						self.lastEtag = newETag;
					}
				},
				{'If-None-Match' : self.lastEtag},
				function(r, status)
				{
					if(status == 304){ // NOT MODIFIED
						// nada
					}else{
						self.defaultErrorCallback(r,status);
					}
				});



				// self.apiGet('/api/v1/event/', function(r)
				// {
				// 	self.all = byId(r);
				// 	self.refreshCurrent();
				// });
				return self;
			}



			// Pone como Evento actual el evento que le pasemos
			this.set		= function(eventId)
			{
				self.currentId = eventId;
				//self.loadSurfers();
				self.loadHeats();
			}

			this.setRound	= function(roundNumber)
			{
				self.currentRoundId = roundNumber;
			}

			this.loadSurfers	= function()
			{
				if(self.currentId)
				self.apiGet('/api/v1/event/'+self.currentId+'/surfers', function(r)
				{
					self.currentSurfers = r;
					self.refreshCurrent();
				});
			}

			this.loadHeats	= function()
			{
				if(self.currentId)
				{
					self.loading = true;
					self.apiGet('/api/v1/event/'+self.currentId+'/heats', function(r)
					{
						var heats = {};
						for(i in r)
						{
							var round = r[i];
							for(j in round.heats)
							{
								var heat = round.heats[j];
								heats[heat.id] = heat;
							}
						}
						self.currentHeats = heats;
						self.refreshCurrent();
						self.loading = false;
						sc.heat.refresh();
					});
				}
			}

			this.loadHeatsIntervalFunction = function ()
			{
				if(self.currentId)
					self.apiGet('/api/v1/event/'+self.currentId+'/heats',
					function(r, status, headers)
					{
						var newETag = false;
						if(headers().etag)
							newETag = headers().etag;

						if(!newETag || newETag != self.lastHeatsEtag)
						{
							var heats = {};
							for(i in r)
							{
								var round = r[i];
								for(j in round.heats)
								{
									var heat = round.heats[j];
									heats[heat.id] = heat;
									//heats[heat.id].round_number = round.number;
								}
							}
							self.currentHeats = heats;
							self.refreshCurrent();
							self.loading = false;
							sc.heat.refresh();

							// Actualizamos etag
							self.lastHeatsEtag = newETag;
						}
					},
					{'If-None-Match' : self.lastHeatsEtag},
					function(r, status)
					{
						if(status == 304){ // NOT MODIFIED
							// nada
						}else{
							self.defaultErrorCallback(r,status);
						}
					});
			}

			this.delHeat	= function(heatId)
			{
				self.apiDelete('/api/v1/heat/'+heatId, function(r)
				{
					self.loadHeats();
				});
			}


			this.new		= function(data)
			{
				var data2 = jQuery.extend(true, {}, data);
				data2.start_date = time.stringToUnix(data.start_date);
				data2.finish_date = time.stringToUnix(data.finish_date);
				self.apiPost('/api/v1/event', data2, function(r)
				{
					self.loadAll()
				});
			}

			this.createRounds	= function(number)
			{
				self.apiPut('/api/v1/event/'+self.currentId+'/round',{rounds_count:number}, function(r)
				{
					self.loadHeats();
				});

			}



		// Privados


			this.refreshCurrent		= function()
			{
				var eventId = self.currentId;
				if(!eventId || self.all[eventId] == undefined)
				{
					self.current 		= false;
				}else{
					self.current = self.all[eventId];
				}

				// var roundId = self.currentRoundId;
				// if(!eventId || !roundId || self.currentRounds[roundId] == undefined)
				// {
				// 	self.currentRound	= false;
				// 	self.currentHeats	= [];
				// }else{
				// 	self.currentRound	= self.currentRounds[roundId];
				// 	self.currentHeats	= self.currentRound.heats;
				// }
			}






	};

	return (Event);
}]);
