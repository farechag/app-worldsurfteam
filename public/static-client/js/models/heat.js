app.factory("Heat", ['$http', '$rootScope', 'Observe', 'ApiFunctions', function($http, $rootScope, Observe, ApiFunctions) {
	var Heat = function()
	{
		var self = this;
		var sc	 = $rootScope;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


		// Variables

			this.data			= [];
			this.lastEtag	= '';

			this.id				= false;
			this.participations	= [];

			this.duration 		= undefined;

		// Propiedades

			this.elapsedPer		= function()
			{
				if(self.duration == undefined)
					return 0;
				if(self.data == undefined || self.data.start_date == undefined)
					return 0;

				elapsed = time.now() - self.data.start_date;

				percentage = Math.round((elapsed/self.duration) * 100);

				if(percentage < 0)
					percentage = 0;
				else if(percentage > 100)
					percentage = 100;


				return percentage;
			}


		// Métodos

			this.set		= function(heatId)
			{
				self.id = heatId;
				self.data = sc.event.currentHeats[self.id];
				if(self.data == undefined)
					self.load();

				self.refresh();
				sc.wave.load();
			}

			this.load		= function()
			{
				if(self.id)
					self.apiGet('/api/v1/heat/'+self.id,
					function(r, status, headers)
					{
						var newETag = false;
						if(headers().etag)
							newETag = headers().etag;

						if(!newETag || newETag != self.lastEtag)
						{
							self.data = r;
							sc.event.currentHeats[self.id] = r;
							self.lastEtag = newETag;
							self.refresh();
						}
					},
					{'If-None-Match' : self.lastEtag},
					function(r, status)
					{
						if(status == 304){ // NOT MODIFIED
							// nada
						}else{
							self.defaultErrorCallback(r,status);
						}
					});



				// self.apiGet('/api/v1/heat/'+self.id, function(r)
				// {
				// 	self.data = r;
				// 	self.refresh();
				// });
			}

			this.addSurfer	= function(surferId)
			{
				self.apiPut('/api/v1/heat/'+self.id+'/surfer/'+surferId, [], function(r)
				{
					self.load();
				});
			}

			this.new	= function(surfers)
			{
				self.apiPost('/api/v1/heat',{round_id:sc.event.currentRoundId,surfers:surfers}, function(r)
				{
					sc.event.loadHeats();
					$('#modal_create_heat').modal('hide');
				});
			}

			this.del	= function(heatId)
			{
				self.apiDelete('/api/v1/heat/'+heatId, function(r)
				{
					sc.event.loadHeats();
				});
			}

			this.delSurfer	= function(surferId)
			{
				self.apiDelete('/api/v1/heat/'+self.id+'/surfer/'+surferId, function(r)
				{
					self.load();
				});
			}

			this.setOfficialScore = function(surferId, score)
			{
				self.apiPut('/api/v1/heat/'+self.id+'/surfer/'+surferId+'/score', {score:score}, function(r)
				{
					self.load();
				});
			}

			this.updateStatus = function(heatId, status)
			{
				self.apiPut('/api/v1/heat/'+heatId+'/status', {status:status}, function(r)
				{
					sc.event.loadHeats();
					self.load();
				});
			}

			this.forceUpdateResults = function()
			{
				sc.forceUpdateScores_status = 'sending';
				self.apiPut('/api/v1/heat/'+self.id+'/calculate_results', [], function(r)
				{
					self.load();
					sc.forceUpdateScores_status = 'ok';
					setTimeout(function()
					{
						sc.forceUpdateScores_status = false;
					}, 2000);
				});
			}







		// Privados


			this.refresh		= function()
			{
				if(self.id)
				{
					var heat = self.data
					if(heat != undefined)
					{
						self.duration = heat.finish_date - heat.start_date;
					}
				}


				if(self.data)
					self.participations = self.data.participations;
				else{
					self.participations = [];
				}
				sc.forceUpdateUnix();

				// var heatId = self.currentId;
				// if(!heatId || self.all[eventId] == undefined)
				// {
				// 	self.current 		= false;
				// 	self.currentSurfers	= [];
				// }else{
				// 	self.current = self.all[eventId];
				// }
				//
				// var roundId = self.currentRoundId;
				// if(!eventId || !roundId || self.currentRounds[roundId] == undefined)
				// {
				// 	self.currentRound	= false;
				// 	self.currentHeats	= [];
				// }else{
				// 	self.currentRound	= self.currentRounds[roundId];
				// 	self.currentHeats	= self.currentRound.heats;
				// }
			}






	};

	return (Heat);
}]);
