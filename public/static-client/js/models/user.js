app.factory("User", ['$http', 'Observe', 'ApiFunctions', '$rootScope', function($http, Observe, ApiFunctions, $rootScope) {
	var User = function()
	{
		var self = this;
        //angular.extend(this, new Observe());
        angular.extend(this, new ApiFunctions());


        // Variables


            this.data       		= undefined,
			this.top_voted_surfers 	= [];



        // Propiedades

            // Bool : Is logued?
            this.logued    = function()
            {
                return self.data ? true : false;
            }

			// Bool : Is admin?
			this.admin		= function()
			{
				return self.data ? self.data.is_admin : false;
			}


        // Métodos

            this.check      = function()
            {
                self.apiGet('/api/v1/account/session',function(r)
                {
                    self.data = r;
					if(r)
					{
						self.loadTopSurfers();
					}else{
						token           = false;
						self.data       = false;
		                $.jStorage.deleteKey('token');
						$rootScope.user.top_voted_surfers = [];
					}
                });
            }

			this.loadTopSurfers = function()
			{
				self.apiGet('/api/v1/account/top_voted_surfers',function(list)
				{
					self.top_voted_surfers = list;
				}, function(status,r){});
			}

            this.login = function(data)
            {
                self.apiPut('/api/v1/account/session', data, function(r)
                {
					log(r);
                    self.data   = r.user;
                    token       = r.token;
                    $.jStorage.set('token', r.token, {TTL:13*24*60*60*1000});
					self.loadTopSurfers();
					location.href = '#/';
                });
            }

            this.logout     = function()
            {
                token           = false;
                self.data       = false;
                $.jStorage.deleteKey('token');
				self.top_voted_surfers = [];
				location.href = '#/';
            }


			this.register = function(data)
			{
				self.apiPost('/api/v1/account', data, function(r)
                {
                    self.data   = r.user;
                    token       = r.token;
                    $.jStorage.set('token', r.token, {TTL:13*24*60*60*1000});
					self.loadTopSurfers();
					location.href = '#/';
                });
			}
			
			this.reset_password_send = function(data)
			{
				self.apiPut('/api/v1/account/password_reset', data, function(r)
                {
                    alert('We have sent you an email with the confirmation.');
                });

			}

			this.reset_password = function(data)
			{
				if(data.password == undefined || data.password_confirmation == undefined
					|| data.password != data.password_confirmation)
					{
						alert('Password and password confirmation must match.');
					}else{
						self.apiPost('/api/v1/account/password_reset', data, function(r)
		                {
		                    alert('Your password has been changed successfully.');
							data = [];
		                });
					}
			}








    };
     
    // Return a reference to the function
    return (User);
}]);
