var app = angular.module('wst', ['ngRoute', 'ngAnimate']);
app.factory("Observe", ['$http', function($http) {
	var Observe = function()
	{
        var self = this;

        this.observers = [];
        this.watch = function(callback){
            self.observers.push(callback);
        };
        this.callObservers = function(object){
            angular.forEach(self.observers, function(callback){
                callback(object);
            });
        };

	};

	return (Observe);
}]);

app.factory("ApiFunctions", ['$http', '$rootScope', function($http, $rootScope) {
	var ApiFunctions = function()
	{
        var self = this;

        this.defaultErrorCallback   = function(r,status)
        {
			var messageModal = '';
			var title = 'Status: ' + status;
			if(typeof r != 'string')
			{
				for(i in r)
				{
					messageModal += '<h2>'+ i + '</h2>';
					for(j in r[i])
					{
						messageModal += '<h4>'+ r[i][j] + '</h4>';
					}
				}
				title = 'Validating error';
			}else{
				messageModal = r;

				// token inválido, lo borramos
				if(status == 401 && r == 'Invalid Token.')
				{
					token           		= false;
					$rootScope.user.data       = false;
	                $.jStorage.deleteKey('token');
					$rootScope.user.top_voted_surfers = [];
					location.href = '#/';
					return;
				}
			}

			modal.error(title, messageModal);
            console.log('Error: '+status, r);
        }

        this.apiGet = function(resource, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.get(resource+(token?'?token='+token:''))
                .success(funSuccess)
                .error(funError);
        }

		this.apiPost = function(resource, data, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.post(resource+(token?'?token='+token:''), data)
                .success(funSuccess)
                .error(funError);
        }

		this.apiDelete = function(resource, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.delete(resource+(token?'?token='+token:''))
                .success(funSuccess)
                .error(funError);
        }

		this.apiPut = function(resource, data, funSuccess, funError){
            if(funError == undefined)
                funError = self.defaultErrorCallback;
            $http
				.put(resource+(token?'?token='+token:''), data)
                .success(funSuccess)
                .error(funError);
        }
	};

	return (ApiFunctions);
}]);


var time =
{
	// Lo que hay que sumarle a la hora local para estar a la hora del server
	offset		: 0,

	now			: function()
	{
		return this.nowClient() + this.offset;
	},

	nowClient	: function()
	{
		return Math.round(jQuery.now()/1000)
	},

	// Establece la hora del server
	setServer	: function(serverTime)
	{
		this.offset = serverTime - this.nowClient();
	},


	serverToClient : function(serverTime)
	{
		return serverTime - this.offset;
	},

	clientToServer : function(clientTime)
	{
		return clientTime + this.offset;
	},

	unixToString        : function (unix)
	{
		var day = moment.unix(unix);
		return day.format("dddd, MMMM Do YYYY, H:mm:ss");
	},

	timeUntilUnix       : function (unix)
	{
		var string = countdown( unix*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.MONTHS | countdown.YEARS, 1 ).toString();
		if(now() - unix > 0)
			string += ' ago';
		else
			string = 'in ' + string;

		return string;
	},

	stringToUnix		: function(string)
	{
		var d = new Date(string);
		var unix = Math.floor(d.getTime()/1000);

		unix -= new Date().getTimezoneOffset()*60;

		return unix;

	}
}




$.getTime = function(zone, success) {
    var url = 'http://json-time.appspot.com/time.json?tz='
            + zone + '&callback=?';
    $.getJSON(url, function(o){
        success && success(new Date(o.datetime), o);
    });
};


function unixToUntil (unix)
{
	unix = time.serverToClient(unix);
	var string = countdown( unix*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.MONTHS | countdown.YEARS, 1 ).toString();
	if(string)
	{
		if(now() - unix > 0)
			string += ' ago';
		else
			string = 'in ' + string;
	}else{
		string = '';
	}
	return string;
}

var modal =
	{
		error		: function(title, body)
		{
			if(typeof body != 'string')
				body = JSON.stringify(body);
			$('#modal_errors .modal-title').html(title);
			$('#modal_errors .modal-body').html(body);
			$('#modal_errors').modal();
		}
	}

function unixToUntilForHeats (unix, withSign)
{
	if(withSign == undefined)
		withSign = true;

	sign = '';
	elapsed = time.now() - unix;
	if(elapsed < 0)
	{
		elapsed = -elapsed;
		sign = '-';
	}

	if(!withSign)
		sign = '';

	if(isNaN(elapsed))
		return '';

	hours = Math.floor(elapsed/3600);
	elapsed = elapsed - hours*3600;
	minutes = Math.floor(elapsed/60);
	elapsed = elapsed - minutes*60;
	seconds = elapsed;

	if(hours)
		hours = hours + ':';
	else
		hours = '';
	if(minutes >= 10)
		minutes = minutes + ':';
	else if(minutes > 0)
		minutes = '0' + minutes + ':';
	else
		minutes = '0:';

	if(seconds < 10)
		seconds = '0' + seconds;

	return sign + hours + minutes + seconds;
}

function unixToUntilForEvents (unix)
{
	if(unix - time.now() < 10)
		return 'In a few seconds!';

	var string = countdown( time.serverToClient(unix)*1000, null, countdown.SECONDS | countdown.MINUTES | countdown.HOURS | countdown.DAYS | countdown.WEEKS, 2 ).toString();

	return 'In ' + string + '!';

	// Versión manual
	remaining = unix - time.now();


	if(isNaN(remaining))
		return '';

	if(remaining > 30*24*3600) // 1 mes
		return '';

	days = Math.floor(remaining/3600);
	remaining = remaining - days*3600;
	hours = Math.floor(remaining/3600);
	remaining = remaining - hours*3600;
	minutes = Math.floor(remaining/60);
	remaining = remaining - minutes*60;
	seconds = remaining;

	items = [];
	if(days)
		items.push(days + 'days');
	if(hours)
		items.push(hours + 'h');
	if(minutes)
		items.push(minutes + 'm');
	if(seconds && !days)
		items.push(seconds + 's');

	return 'In ' + items.join(' ') + '!';
}

var token = $.jStorage.get('token', false);



// Reordena un Array poniendo como índice los IDs de sus elementos
function byId (array)
{
	var result = {};
	for(var i=0;i<array.length;i++)
	{
		result[array[i].id] = array[i];
	}
	return result;
}
