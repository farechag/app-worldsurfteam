<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use DB;
use App\Wave;
use App\Heat;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();

		$schedule->call(function()
		{

			// Actualiza los scores de las olas. No necesario al 100%, ya que la siguiente linea
			// volverá a hacer los cálculos, pero si queremos mostrar los scores de las
			// las olas antes de que el heat termine sí que la necesitamos
			//
			// Este lo voy a dejar para que el admin vaya viendo los scores de las olas
			// echo 'Waves calculadas: ' . (Wave::onlyFinished()->onlyWithoutScore()->updateScore() - 1) . "\n";


			// Finalmente calcularé el score provisional y el #votos de todas las olas
			// actualmente activas, así el admin/usuarios pueden ver las puntuaciones y el # votos
			echo 'Waves calculadas: ' . (Wave::onlyActives()->updateScore() - 1) . "\n";

			// Calcula los scores de los heats que hayan terminado y que tengan
			// cosas aun sin calcular. Tampoco es necesaria porque cuando terminamos un heat le calculamos los resultados.
			// echo 'Heats calculados: ' . (Heat::onlyFinished()->onlyNotFullCalculated()->updateScore() - 1) . "\n";

			// Ejecutamos cada minuto
		})->cron('* * * * *');
	}

}
