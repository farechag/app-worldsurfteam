<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		//'App\Http\Middleware\CatchExceptions'
	//	'App\Http\Middleware\VerifyCsrfToken',
	/*
	*	CSRF desactivado por defecto, activarlo en las rutas públicas
	*/
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'null' 			=> 'App\Http\Middleware\Null',
		'auth' 			=> 'App\Http\Middleware\Authenticate',
		'auth-forced'	=> 'App\Http\Middleware\ForceAuthenticated',
		'auth-forced-no'=> 'App\Http\Middleware\ForceNoAuthenticated',
		'auth-only-admin'=> 'App\Http\Middleware\ForceAdmin',
		'check-input'	=> 'App\Http\Middleware\CheckInput',
	//	'auth.basic' 	=> 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'csrf'  		=> 'Illuminate\Foundation\Http\Middleware\VerifyCsrfToken',


	];





}
