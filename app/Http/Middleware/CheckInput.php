<?php namespace App\Http\Middleware;

use Closure;
use App\Http\Responses\Output;

use App\Event;
use App\Heat;
use App\Surfer;
use App\Round;

class CheckInput
{

	protected $output;
	public function __construct(Output $output)
	{
		$this->output = $output;
	}

	/**
	 * Comprueba que todo elemento que se le pase exista de verdad.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->has('event_id'))
			if(is_null(Event::find($request->input('event_id'))))
				return $this->output->error(404, 'Evento no encontrado.');

		if ($request->has('round_id'))
			if(is_null(Round::find($request->input('round_id'))))
				return $this->output->error(404, 'Round no encontrado.');

		if ($request->has('heat_id'))
			if(is_null(Heat::find($request->input('heat_id'))))
				return $this->output->error(404, 'Heat no encontrado.');

		if ($request->has('surfer_id'))
			if(is_null(Surfer::find($request->input('surfer_id'))))
				return $this->output->error(404, 'Surfer no encontrado.');

		if ($request->has('wave_id'))
			if(is_null(Wave::find($request->input('wave_id'))))
				return $this->output->error(404, 'Wave no encontrada.');


		return $next($request);
	}

}
