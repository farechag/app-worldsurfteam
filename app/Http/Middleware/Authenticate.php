<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Responses\Output;
use JWTAuth;
use Auth;

class Authenticate {

	/**
	 * Obtiene la información del usuario en base al TOKEN y
	 *
	 * @var Guard
	 */
	protected $auth;
	protected $output;
	public $user;
	public $tokenError;
	public function __construct(Guard $auth, Output $output)
	{
		$this->auth 	= $auth;
		$this->output 	= $output;

		// User
		$user       = false;
		$tokenError = false;
		try
		{
			$user = JWTAuth::parseToken()->toUser();
			Auth::loginUsingId($user->id);
		}
		catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			$tokenError = 'invalid';                                                // Token recibido pero inválido. Puede ser cualquier texto.
		}
		catch(\Tymon\JWTAuth\Exceptions\InvalidClaimException $e)
		{
			$tokenError = 'claim';
		}
		catch(\Tymon\JWTAuth\Exceptions\PayloadException $e)
		{
			$tokenError = 'payload';
		}
		catch(\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e)
		{
			$tokenError = 'blacklisted';
		}
		catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			$tokenError = 'expired';                                                // Token recibido pero caducado
		}
		catch(\Exception $e)
		{
			$message = $e->getMessage();
			if($message == 'The token could not be parsed from the request')
				$tokenError = 'absent';                                             // No han mandado nada
		}

		$this->user 		= $user;
		$this->tokenError 	= $tokenError;

	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		switch($this->tokenError)
		{
			// case 'expired22':
			//
			// 	// Vamos a ver si está caducado pero podemos hacer Refresh
			// 	if(JWTAuth::getToken())
			// 	{
			// 		try
			// 		{
			// 			$newToken = JWTAuth::parseToken()->refresh();
			// 			$tokenError = 'wuww';
			// 		}
			// 		catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
			// 		{
			// 			$tokenError = 'invalid';                                                // Token recibido pero inválido. Puede ser cualquier texto.
			// 		}
			// 		catch(\Tymon\JWTAuth\Exceptions\InvalidClaimException $e)
			// 		{
			// 			$tokenError = 'claim';
			// 		}
			// 		catch(\Tymon\JWTAuth\Exceptions\PayloadException $e)
			// 		{
			// 			$tokenError = 'payload';
			// 		}
			// 		catch(\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e)
			// 		{
			// 			$tokenError = 'blacklisted';
			// 		}
			// 		catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
			// 		{
			// 			$tokenError = 'expired';                                                // Token recibido pero caducado
			// 		}
			// 		catch(\Exception $e)
			// 		{
			// 			$tokenError = 'e';
			// 		}
			// 		return $this->output->error(401, 'cosa: ' . $tokenError);
			// 	}
			// 		return $this->output->error(401, 'cosa');
			case 'invalid':
			case 'expired':
				return $this->output->error(401, 'Invalid Token.');
			default:
		}

		/*
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->guest('auth/login');
			}
		}
		*/

		return $next($request);
	}

}
