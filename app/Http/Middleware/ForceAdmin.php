<?php namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Auth;
use App\Http\Middleware\Authenticate;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Responses\Output;

class ForceAdmin extends Authenticate
{

	public function __construct(Guard $auth, Output $output)
	{
		parent::__construct($auth, $output);
	}

	/**
	 * Comprueba que seas Admin y si no lo eres devuelve 'NOT ALLOWED'
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Auth::isAdmin())
		{
			if($request->ajax())
				return $this->output->error(403, 'Sin permiso.');
			return Response('Not allowed', 403);
		}

		return $next($request);
	}

}
