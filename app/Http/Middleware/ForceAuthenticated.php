<?php namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Auth;
use App\Http\Middleware\Authenticate;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Responses\Output;

class ForceAuthenticated extends Authenticate
{

	public function __construct(Guard $auth, Output $output)
	{
		parent::__construct($auth, $output);
	}

	/**
	 * Fuerza a que estés logueado.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!$this->user)
			return $this->output->error(403, 'You need to be logued to continue.');

		return $next($request);
	}

}
