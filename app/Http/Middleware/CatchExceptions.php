<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Responses\Output;
use JWTAuth;
use Auth;
use Symfony\Component\Debug\Exception\FatalErrorException;

class CatchExceptions {


	protected $output;
	public function __construct(Output $output)
	{
		$this->output = $output;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		try{
			$response = $next($request);
		}catch(\Exception $e)
		{
			return $this->output->error(400, 0, $e->getMessage(). "\n\n" . $e->getFile() . ':' . $e->getLine());
		}

		return $response;
	}



}
