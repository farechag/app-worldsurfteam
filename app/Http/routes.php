<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 		'WebController@client');
Route::get('/admin', 	'WebController@admin');

Route::get('/ping', function(){ return 'ok'; });

Route::get('/env', function(){ return App::environment(); });

// Route::controllers([
// 	'password' 	=> 'Auth\PasswordController'
// ]);

// API

Route::group(['prefix' => 'api', 'middleware' => 'auth'], function()
{
	Route::group(['prefix' => 'v1'], function ()
	{
		/* SERVER */

			Route::get(		'server/time', 								'ServerController@get_time');
			Route::get(		'server/email', 							'ServerController@test_email');

		/* ACCOUNT */

			Route::get(		'account/session', 							'AccountController@check');
			Route::put(		'account/session', 							'AccountController@login');
			Route::post(	'account', 									'AccountController@register');
			Route::get( 	'account/top_voted_surfers', 				'AccountController@get_top_voted_surfers');
			Route::put(		'account/password_reset',					'AccountController@forgot_password_send_link');
			Route::post(	'account/password_reset',					'AccountController@forgot_password_apply');

		/* COUNTRY */

			Route::get(		'country', 									'CountryController@get_all');
			Route::get(		'country/{country_id}',						'CountryController@get_country');

		/* SURFER */

			Route::get(		'surfer',									'SurferController@get_all');
			Route::post(	'surfer',									'SurferController@new_surfer');
			Route::put(		'surfer/{surfer_id}',						'SurferController@modify_surfer');
			Route::delete(	'surfer/{surfer_id}',						'SurferController@del_surfer');

		/* EVENT */

			Route::get(		'event', 									'EventController@get_all');
			Route::get(		'event/get_sponsors', 						'EventController@get_sponsors');
			Route::get(		'event/get_types',	 						'EventController@get_types');
			Route::post(	'event', 									'EventController@new_event');
			Route::get(		'event/{event_id}', 						'EventController@get_event');
			Route::put(		'event/{event_id}/round',					'EventController@set_rounds');
			Route::get(		'event/{event_id}/heats',					'EventController@get_heats');
			Route::get(		'event/{event_id}/surfers',					'EventController@get_surfers');
			Route::put(		'event/{event_id}/surfer/{surfer_id}', 		'EventController@add_surfer');
			Route::delete(	'event/{event_id}/surfer/{surfer_id}', 		'EventController@del_surfer');

		/* HEAT */

			Route::post(	'heat',										'HeatController@new_heat');
			Route::get(		'heat/{heat_id}',							'HeatController@get_heat');
			Route::delete(	'heat/{heat_id}',							'HeatController@del_heat');
			Route::put(		'heat/{heat_id}/status',					'HeatController@set_status');
			Route::get(		'heat/{heat_id}/waves',						'HeatController@get_waves');
			Route::put(		'heat/{heat_id}/calculate_results',			'HeatController@calc_results');
			Route::put(		'heat/{heat_id}/surfer/{surfer_id}',		'HeatController@add_surfer');
			Route::delete(	'heat/{heat_id}/surfer/{surfer_id}',		'HeatController@del_surfer');
			Route::put(		'heat/{heat_id}/surfer/{surfer_id}/score',	'HeatController@set_score');

		/* WAVE */

			Route::post(	'wave', 									'WaveController@new_wave');
			Route::delete(	'wave/{wave_id}', 							'WaveController@del_wave');
			Route::put(		'wave/{wave_id}/vote', 						'WaveController@vote');
			Route::put(		'wave/{wave_id}/finish',					'WaveController@finish_wave');

	});

});
