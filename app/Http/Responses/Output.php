<?php namespace App\Http\Responses;

use League\Fractal\Manager as Fractal;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class Output {

    /**
     * Fractal library used for data presention and transformation.
     *
     * @var League\Fractal\Manager
     */
    private $fractal;

    /**
     * Construct a new output manager with a fractal manager.
     *
     * @param League\Fractal\Manager $fractal
     */
    public function __construct(Fractal $fractal)
    {
        $this->fractal = $fractal;
    }


    public function applyCallback ($object, $callback)
    {
        try{
            $resource   = new Item($object, $callback);
            $root       = $this->fractal->createData($resource);
            $object     = $root->toArray()['data'];
        }catch(\ErrorException $e)
        {
            $resource   = new Collection($object, $callback);
            $root       = $this->fractal->createData($resource);
            $object     = $root->toArray()['data'];
        }
        return $object;
    }

    /**
     * Devuelve un resultado de éxito por la API
     *
     * @param mixed data        you want to send
     * @param bool  isOneItem   Is collection or only one object?
     * @param func  callback    Function for each item
     **/
    public function success($data = true, $callback = null, $headers = Array())
    {
        if(!is_null($callback))
            $data = $this->applyCallback($data, $callback);

        return response()->json($data, 200, $headers);
    }

    public function notModified()
    {
        return response()->json([], 304);
    }

    /**
     * Devuelve un resultado de error por la API
     *
     * @param mixed data        you want to send
     * @param bool  isOneItem   Is collection or only one object?
     * @param func  callback    Function for each item
     **/
    public function error ($httpCode = 400, $message = '', $headers = Array())
    {
        return response()->json($message, $httpCode, $headers);
    }



}
