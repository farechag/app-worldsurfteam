<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;

use Validator;
use Input;
use Auth;

use App\Surfer;

class SurferController extends ApiController
{


		public function __construct(Output $output)
		{
			parent::__construct($output);

			$this->middleware('auth-only-admin', ['except' => ['get_all']]);
			$this->middleware('check-input');
		}

		public function get_all(Request $request)
		{
			//Obtenemos la lista de surfers

				$surfers = Surfer::all();


			return $this->success($surfers);
		}


		public function new_surfer(Request $request)
		{
				$validator = Validator::make(Input::all(), [
					'name'			=> 'required|string',
					'lastname'		=> 'required|string',
					'nickname'		=> 'string',
					'country_id'	=> 'integer',
					'wsl'			=> '',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Cargamos la info

				$data = [];
				$data['name']		= Input::get('name', '');
				$data['lastname']	= Input::get('lastname', '');
				$data['nickname']	= Input::get('nickname', '');
				$data['country_id']	= Input::get('country_id', null);

				$wsl	= Input::get('wsl', 0);
				$wsl	= is_string($wsl) ? strtolower($wsl) : $wsl;
				switch($wsl)
				{
					case 1:
					case '1':
					case 'true':
						$wsl = true;
						break;
					default:
						$wsl = false;
				}
				$data['wsl']		= $wsl;

			// Guardamos

				$surfer = Surfer::create($data);


			return $this->success();
		}


		public function modify_surfer(Request $request, $surfer_id)
		{
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');
				$validator = Validator::make(Input::all(), [
					'name'			=> 'string',
					'lastname'		=> 'string',
					'nickname'		=> 'string',
					'country_id'	=> 'integer',
					'wsl'			=> '',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Cargamos la info

				$data = [];
				if(Input::has('name'))	$data['name'] = Input::get('name');
				if(Input::has('lastname'))	$data['lastname'] = Input::get('lastname');
				if(Input::has('nickname'))	$data['nickname'] = Input::get('nickname');
				if(Input::has('country_id'))	$data['country_id'] = Input::get('country_id');
				if(Input::has('wsl'))
				{
					$wsl	= Input::get('wsl');
					$wsl	= is_string($wsl) ? strtolower($wsl) : $wsl;
					switch($wsl)
					{
						case 1:
						case '1':
						case 'true':
							$wsl = true;
							break;
						default:
							$wsl = false;
					}
					$data['wsl']		= $wsl;
				}

			// Guardamos

				Surfer::find($surfer_id)->update($data);


			return $this->success();
		}

		public function del_surfer(Request $request, $surfer_id)
		{
			// Comprobamos que exista

				$surfer = Surfer::find($surfer_id);
				if(is_null($surfer))
					return $this->error(404, 'Surfer not found.');

			// Lo borramos

				$surfer->delete();


			return $this->success();
		}



}
