<?php

	use Swagger\Annotations as SWG;

	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Server",
	*		@SWG\Api(
	*			path="server/time",
	*			@SWG\Operation(
	*				method="Get",
	*				type="array",
	*				summary="Obtiene el timestamp del server",
	*				nickname="get_time",
	*			)
	*		)
	* )
	*/

	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Account",
	*		@SWG\Api(
	*			path="account",
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="Registra a un usuario.",
	*				nickname="register",
	*				@SWG\Parameter(
	*					name="name",
	*					description="Nombre del usuario",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Juan García"
	*				),
	*				@SWG\Parameter(
	*					name="email",
	*					description="Email del usuario",
	*					paramType="form",
	*					type="email",
	*					defaultValue="ana@ana.com"
	*				),
	*				@SWG\Parameter(
	*					name="password",
	*					description="Contraseña del usuario",
	*					paramType="form",
	*					type="password",
	*					defaultValue="secret"
	*				),
	*				@SWG\Parameter(
	*					name="country_id",
	*					description="ID del país",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito"),
	*				@SWG\ResponseMessage(code=400, message="Email en uso. (o datos inválidos)"),
	*				@SWG\ResponseMessage(code=403, message="Ya logueado."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="account/session",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Loguea al usuario. Devuelve el token y la info del usuario.",
	*				nickname="login",
	*				@SWG\Parameter(
	*					name="email",
	*					description="Email del usuario",
	*					paramType="form",
	*					type="email",
	*					defaultValue="ana@ana.com"
	*				),
	*				@SWG\Parameter(
	*					name="password",
	*					description="Contraseña del usuario",
	*					paramType="form",
	*					type="password",
	*					defaultValue="secret"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=400, message="Email o usuario incorrecto."),
	*				@SWG\ResponseMessage(code=403, message="Ya logueado."),
	*			),
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Comprueba el token y devuelve el usuario (si lo hay) con esa sesión.",
	*				nickname="check",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="account/password_reset",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Envía un correo al usuario para poder resetear su contraseña.",
	*				nickname="forgot_password_send_link",
	*				@SWG\Parameter(
	*					name="email",
	*					description="Email del usuario",
	*					paramType="form",
	*					type="email",
	*					defaultValue="ana@ana.com"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Email incorrecto."),
	*				@SWG\ResponseMessage(code=403, message="Ya logueado."),
	*			),
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="Resetea tu contraseña en base al token enviado por email.",
	*				nickname="forgot_password_apply",
	*				@SWG\Parameter(
	*					name="token",
	*					description="Token",
	*					paramType="form",
	*					type="string",
	*					defaultValue="db946f9b2fad87d07f0e840341f098c19fdcda75"
	*				),
	*				@SWG\Parameter(
	*					name="email",
	*					description="Email del usuario",
	*					paramType="form",
	*					type="email",
	*					defaultValue="ana@ana.com"
	*				),
	*				@SWG\Parameter(
	*					name="password",
	*					description="Nueva contraseña.",
	*					paramType="form",
	*					type="password",
	*					defaultValue="secret"
	*				),
	*				@SWG\Parameter(
	*					name="password_confirmation",
	*					description="Repite contraseña.",
	*					paramType="form",
	*					type="password",
	*					defaultValue="secret"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Email incorrecto."),
	*				@SWG\ResponseMessage(code=403, message="Ya logueado."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="account/top_voted_surfers",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Devuelve la lista de surfers más votados por este usuario.",
	*				nickname="get_top_voted_surfers",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			)
	*		)
	* )
	*/




	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Event",
	*		@SWG\Api(
	*			path="event",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de eventos.",
	*				nickname="get_events",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			),
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="[NOT IMPLEMENTED YET] Crea un evento.",
	*				nickname="new_event",
	*				@SWG\Parameter(
	*					name="name",
	*					description="Nombre del evento",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Evento molón de surf"
	*				),
	*				@SWG\Parameter(
	*					name="location",
	*					description="Lugar del evento",
	*					paramType="form",
	*					type="string",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="start_date",
	*					description="Fecha de inicio (timestamp)",
	*					paramType="form",
	*					type="string",format="date",
	*					defaultValue="1424786954"
	*				),
	*				@SWG\Parameter(
	*					name="finish_date",
	*					description="Fecha de fin (timestamp)",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1434786954"
	*				),
	*				@SWG\Parameter(
	*					name="country_id",
	*					description="ID del país",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="type_id",
	*					description="Tipo de evento",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="sponsor_id",
	*					description="El esponsor del evento",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito"),
	*				@SWG\ResponseMessage(code=400, message="Datos inválidos."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/{event_id}",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene un evento.",
	*				nickname="get_event",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event desconocido."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/get_sponsors",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de sponsors.",
	*				nickname="get_sponsors",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/get_types",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de event types.",
	*				nickname="get_types",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/{event_id}/surfer/{surfer_id}",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Inscribe a un surfer a un evento.",
	*				nickname="add_surfer",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event o Surfer desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			),
	*			@SWG\Operation(
	*				method="DELETE",
	*				type="array",
	*				summary="Quita a un surfer de un evento.",
	*				nickname="del_surfer",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event o Surfer desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/{event_id}/heats",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene los heats de un evento.",
	*				nickname="get_heats",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event desconocido."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/{event_id}/round",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Establece las rondas iniciales de un evento.",
	*				nickname="get_heats",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="rounds_count",
	*					description="Número total de rondas",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="6"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event desconocido."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="event/{event_id}/surfers",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene los surfers de un evento.",
	*				nickname="get_heats",
	*				@SWG\Parameter(
	*					name="event_id",
	*					description="ID del evento",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Event desconocido."),
	*			)
	*		),
	* )
	*/

	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Heat",
	*		@SWG\Api(
	*			path="heat",
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="Crea un heat nuevo.",
	*				nickname="new_heat",
	*				@SWG\Parameter(
	*					name="round_id",
	*					description="ID del round",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfers",
	*					description="Array de las ID's de los surfers.",
	*					paramType="form",
	*					type="array",
	*					@SWG\Items("integer"),
	*					defaultValue="[]"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Round desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/calculate_results",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Fuerza a que se calculen los resultados de un heat (para no esperar 1 minuto)",
	*				nickname="calculate_results",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*			),
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene un heat.",
	*				nickname="get_heat",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*			),
	*			@SWG\Operation(
	*				method="DELETE",
	*				type="array",
	*				summary="Borra un heat.",
	*				nickname="del_heat",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/status",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Actualiza el estado de un heat. Para encenderlo y apagarlo.",
	*				nickname="update_heat",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="status",
	*					description="Nuevo estado del heat.",
	*					paramType="form",
	*					type="boolean",
	*					enum="['on', 'off']",
	*					defaultValue="on"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/surfer/{surfer_id}",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Inscribe a un surfer en un heat.",
	*				nickname="add_surfer",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat o Surfer desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			),
	*			@SWG\Operation(
	*				method="DELETE",
	*				type="array",
	*				summary="Quita a un surfer de un heat.",
	*				nickname="del_surfer",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat o Surfer desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/surfer/{surfer_id}/score",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Le actualiza el score oficial a un surfer en los resultados de un heat.",
	*				nickname="set_surfer_score",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="score",
	*					description="Puntos oficiales",
	*					paramType="form",
	*					type="numeric",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat o Surfer desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			),
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/waves",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de Waves de un heat.",
	*				nickname="get_waves",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="Id del Heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="heat/{heat_id}/surfers",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de Surfers de un heat.",
	*				nickname="get_surfers",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="Id del Heat",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*			)
	*		)
	* )
	*/



	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Country",
	*		@SWG\Api(
	*			path="country",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de países",
	*				nickname="get",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="country/{country_id}",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la información detallada de un país.",
	*				nickname="get_country",
	*				@SWG\Parameter(
	*					name="country_id",
	*					description="Id del country",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Country desconocido."),
	*			)
	*		),
	* )
	*/

	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Surfer",
	*		@SWG\Api(
	*			path="surfer",
	*			@SWG\Operation(
	*				method="GET",
	*				type="array",
	*				summary="Obtiene la lista de surfers.",
	*				nickname="get_all",
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*			),
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="Añade a un surfer.",
	*				nickname="new_surfer",
	*				@SWG\Parameter(
	*					name="name",
	*					description="Nombre",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Kelly"
	*				),
	*				@SWG\Parameter(
	*					name="lastname",
	*					description="Lastname",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Slater"
	*				),
	*				@SWG\Parameter(
	*					name="nickname",
	*					description="Nickname",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Kelly S."
	*				),
	*				@SWG\Parameter(
	*					name="country_id",
	*					description="ID del país",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="wsl",
	*					description="Es WSL?",
	*					paramType="form",
	*					type="boolean",
	*					defaultValue="true"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="surfer/{surfer_id}",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Modifica la información de un surfer",
	*				nickname="modify_surfer",
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="name",
	*					description="Nombre",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Kelly"
	*				),
	*				@SWG\Parameter(
	*					name="lastname",
	*					description="Lastname",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Slater"
	*				),
	*				@SWG\Parameter(
	*					name="nickname",
	*					description="Nickname",
	*					paramType="form",
	*					type="string",
	*					defaultValue="Kelly S."
	*				),
	*				@SWG\Parameter(
	*					name="country_id",
	*					description="ID del país",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="wsl",
	*					description="Es WSL?",
	*					paramType="form",
	*					type="boolean",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="surfer/{surfer_id}",
	*			@SWG\Operation(
	*				method="DELETE",
	*				type="array",
	*				summary="Elimina a un surfer.",
	*				nickname="modify_surfer",
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		)
	* )
	*/


	/**
	* @SWG\Resource(
	*		basePath="/api/v1/",
	*		resourcePath="Wave",
	*		@SWG\Api(
	*			path="wave/{wave_id}/vote",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Vota una Wave.",
	*				nickname="vote",
	*				@SWG\Parameter(
	*					name="wave_id",
	*					description="ID del wave",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="points",
	*					description="Puntos de la Wave.",
	*					paramType="form",
	*					type="float",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Wave desconocida."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="wave/{wave_id}/finish",
	*			@SWG\Operation(
	*				method="PUT",
	*				type="array",
	*				summary="Termina una Wave.",
	*				nickname="vote",
	*				@SWG\Parameter(
	*					name="wave_id",
	*					description="ID del wave",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Wave desconocida."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="wave/{wave_id}",
	*			@SWG\Operation(
	*				method="DELETE",
	*				type="array",
	*				summary="Borra una Wave.",
	*				nickname="del_wave",
	*				@SWG\Parameter(
	*					name="wave_id",
	*					description="ID del wave",
	*					paramType="path",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Wave desconocida."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		),
	*		@SWG\Api(
	*			path="wave",
	*			@SWG\Operation(
	*				method="POST",
	*				type="array",
	*				summary="Crea una wave.",
	*				nickname="new_wave",
	*				@SWG\Parameter(
	*					name="heat_id",
	*					description="ID del evento",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="surfer_id",
	*					description="ID del surfer.",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="1"
	*				),
	*				@SWG\Parameter(
	*					name="duration",
	*					description="Duración en segundos. Si no se envía se aplican el default de 3 minutos.",
	*					paramType="form",
	*					type="integer",
	*					defaultValue="180"
	*				),
	*				@SWG\ResponseMessage(code=200, message="Éxito."),
	*				@SWG\ResponseMessage(code=404, message="Heat desconocido."),
	*				@SWG\ResponseMessage(code=403, message="Sin permiso."),
	*			)
	*		)
	* )
	*/
