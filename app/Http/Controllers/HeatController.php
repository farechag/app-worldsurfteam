<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;

use Validator;
use Input;
use Hash;
use Auth;
use DB;

use	App\User;
use App\Event;
use App\Surfer;
use App\HeatParticipation;
use App\Heat;
use App\Vote;
use App\Round;
use App\Wave;

class HeatController extends ApiController
{

		public function __construct(Output $output)
		{
			parent::__construct($output);

			$this->middleware('auth-only-admin', ['except' => ['get_heat', 'get_waves']]);
			$this->middleware('check-input');
		}

		public function set_score(Request $request, $heat_id, $surfer_id)
		{
				if(is_null(Heat::find($heat_id)))
					return $this->error(404, 'Heat not found.');
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');
				$validator = Validator::make(Input::all(), [
					'score'		=> 'required|numeric|min:0|max:20',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Obtenemos el heatParticipation

				$heatParticipation = HeatParticipation::where([	'heat_id'	=> $heat_id,
																'surfer_id'	=> $surfer_id])
														->first();
				if(!$heatParticipation)
					return $this->error(400, 'Surfer not associated with this heat.');

			// Le ponemos la nueva nota

				$heatParticipation->score_asp = Input::get('score');
				$heatParticipation->save();


			return $this->success();
		}

		public function calc_results(Request $request, $heat_id)
		{
			// Obtenemos el heat

				$heat = Heat::whereId($heat_id);

			// Comprobamos

				if(is_null($heat))
					return $this->error(404, 'Heat not found.');

			// Lanzamos el update

				$heat->updateScore();


			return $this->success();
		}

		public function add_surfer(Request $request, $heat_id, $surfer_id)
		{
				if(is_null(Heat::find($heat_id)))
					return $this->error(404, 'Heat not found.');
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');

			// Ya asociado al heat?

				$exists = HeatParticipation::where([	'heat_id'	=> $heat_id,
														'surfer_id'	=> $surfer_id])
														->count();
				if($exists)
					return $this->error(403, 'Surfer already associated with this heat.');

			// Le unimos

				HeatParticipation::create([	'heat_id'	=> $heat_id,
											'surfer_id'	=> $surfer_id]);


			return $this->success();
		}

		public function del_surfer(Request $request, $heat_id, $surfer_id)
		{
				if(is_null(Heat::find($heat_id)))
					return $this->error(404, 'Heat not found.');
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');

			// Surfer no unido a este evento?

				$exists = HeatParticipation::where([	'heat_id'	=> $heat_id,
														'surfer_id'	=> $surfer_id])
														->count();
				if(!$exists)
					return $this->error(403, 'Surfer not associated with this heat.');

			// Le quitamos del evento

				HeatParticipation::where([	'heat_id'	=> $heat_id,
											'surfer_id'	=> $surfer_id])
											->delete();


			return $this->success();
		}

		public function new_heat(Request $request)
		{
				$validator = Validator::make(Input::all(), [
					'round_id' 		=> 'required|integer',
					'surfers'		=> '',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());


			// Comprobamos surfers

				$surfers = Input::get('surfers', []);
				if(is_string($surfers) && strlen($surfers)>0)
					$surfers = explode(',', $surfers);
				foreach($surfers as $surfer_id)
				{
					if(is_null(Surfer::find($surfer_id)))
						return $this->error(404, 'Surfer not found.');
				}

			// Obtenemos el número que le toca

				$heats = Heat::where(Input::only('round_id'))->count();
				$number = $heats+1;

			// Guardamos el heat

				$heat = Heat::create(['round_id'=>Input::get('round_id'), 'number'=>$number]);

			// Añadimos los surfers

				$surfersData = [];
				foreach($surfers as $i => $surfer_id)
				{
					$surfersData[] = [ 	'heat_id' 	=> $heat['id'],
										'surfer_id'	=> $surfer_id ];
				}
				if(count($surfersData))
					DB::table('heats_participations')->insert($surfersData);


			return $this->success();
		}

		public function set_status(Request $request, $heat_id)
		{
				$validator = Validator::make(Input::all(), [
					'duration'		=> 'integer|min:1|max:100',
					'status' 		=> 'required|string',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Obtenemos el heat

				$heat = Heat::find($heat_id);

			// Comprobamos que existe

				if(is_null($heat))
					return $this->error(404, 'Heat not found.');

			// Preparamos el cambio

				switch(Input::get('status'))
				{
					case 'on':
						$heat->start_date	= time();
						$heat->finish_date	= time() + 60*Input::get('duration', 30);
						$heat->finished		= false;
						break;
					case 'off':
						$heat->finish_date	= time();
						$heat->finished		= true;
						$heat->updateScore();
						break;
					case 'delay':
						$base = $heat->finished ? time() : $heat->finish_date;
						// $heat->finish_date	= $heat->finish_date + 60*Input::get('duration', 5);
						$heat->finish_date	= $base + 60*Input::get('duration', 5);
						$heat->finished		= false;
						break;
					default:
						return $this->error(400, 'Status unknown.');
				}

			// Guardamos

				$heat->save();


			return $this->success();
		}

		public function get_heat(Request $request, $heat_id)
		{
			// Obtenemos el heat

				$heat = Heat::find($heat_id);

			// Comprobamos que exista

				if(is_null($heat))
					return $this->error(404, 'Heat not found.');

			// Atributos

				$heat->active();
				$heat->status();
				$heat->participations;
				foreach($heat->participations as $part)
				{
					$part->surfer;
					$part->surfer->country;
				}
				$heat->round;

			// Caché ETAG

				$headers 	= getallheaders();
				$oldEtag	= isset($headers['If-None-Match']) ? $headers['If-None-Match'] : false;
				$newEtag 	= md5(json_encode($heat));
				if($newEtag == $oldEtag)
					return $this->notModified();


			return $this->success($heat, null, ['ETag'=> $newEtag]);
		}

		public function del_heat(Request $request, $heat_id)
		{
			// Obtenemos el heat

				$heat = Heat::find($heat_id);

			// Comprobamos que exista

				if(!$heat->count())
					return $this->error(404, 'Heat not found.');

			// La borramos

				$heat->delete();


			return $this->success();
		}

		public function get_waves(Request $request, $heat_id)
		{
				if(is_null(Heat::find($heat_id)))
					return $this->error(400, 'Heat not found.');

			// Obtenemos las waves

				$waves = Wave::where([	'heat_id'	=> $heat_id])->get();

			// Obtenemos los votos del usuario

				$user = Auth::user();
				$votes = [];
				if($user)
				{
					$votesPoints= Vote::whereUserId($user->id)->orderBy('id', 'asc')->lists('points', 'wave_id');
					$votesDates = Vote::whereUserId($user->id)->orderBy('id', 'asc')->lists('date', 'wave_id');
				}

			// Obtenemos atributos

				foreach ($waves as $wave)
				{
					$wave->surfer;
					$wave->active();
					if(isset($votesPoints[$wave->id]))
					{
						$wave->vote			= $votesPoints[$wave->id];
						$wave->vote_date 	= $votesDates[$wave->id];
					}

				}

			// Caché ETAG

				$headers 	= getallheaders();
				$oldEtag	= isset($headers['If-None-Match']) ? $headers['If-None-Match'] : false;
				$newEtag 	= md5(json_encode($waves));
				if($newEtag == $oldEtag)
					return $this->notModified();


			return $this->success($waves, null, ['ETag'=> $newEtag]);
		}



}
