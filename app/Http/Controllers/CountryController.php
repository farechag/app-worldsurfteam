<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;

use Validator;
use Input;

use	App\Country;


class CountryController extends ApiController
{

	public function __construct(Output $output)
	{
		parent::__construct($output);
	}


	public function get_all (Request $request)
	{
		// Obtenemos la lista de países

			$countries = Country::where('name','<>','')->select('id', 'name')->get();


		return $this->success($countries);
	}

	public function get_country(Request $request, $country_id)
	{
		// Obtenemos el país

			$country = Country::find($country_id);

		// Comprobamos que exista

			if(is_null($country))
				return $this->error(404, 'Country desconocido.');


		return $this->success($country);
	}




}
