<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;

use Illuminate\Http\Response;

use UnexpectedValueException;
use JWTAuth;
use Auth;

class ApiController extends Controller {

    /**
     * An output class for sending expected outputs.
     *
     * @var App\Http\Responses\Output
     */
    private $output;

    public $user;
    public $tokenError;

    /**
     * Make a new api controller with an output class.
     *
     * @param App\Http\Responses\Output $output
     */
    public function __construct(Output $output)
    {
        // Para eliminar las cookies de sesión de laravel
        \Config::set('session.driver', 'array');

        $this->output = $output;
    }

    /**
     * Devuelve un resultado de éxito por la API
     *
     * @param mixed data        you want to send
     * @param bool  isOneItem   Is collection or only one object?
     * @param func  callback    Function for each item
     **/
    public function success($data = true, $callback = null, $headers = Array())
    {
        return $this->output->success($data, $callback, $headers);
    }

    public function notModified()
    {
        return $this->output->notModified();
    }

    /**
     * Devuelve un resultado de error por la API
     *
     * @param mixed data        you want to send
     * @param bool  isOneItem   Is collection or only one object?
     * @param func  callback    Function for each item
     **/
    public function error ($httpCode = 400, $message = '', $headers = Array())
    {
        return $this->output->error($httpCode, $message, $headers);
    }


}
