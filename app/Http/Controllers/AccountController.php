<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Contracts\Auth\Registrar;

use Validator;
use Input;
use Hash;
use Auth;
use DB;
use Mail;

use	App\User;
use	App\Vote;
use	App\Surfer;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;


class AccountController extends ApiController
{
	protected $auth;
	protected $passwords;


	public function __construct(Output $output, Guard $auth, PasswordBroker $passwords)
	{
		parent::__construct($output);
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('check-input');
		$this->middleware('auth-forced', ['only' => ['get_top_voted_surfers']]);
		$this->middleware('auth-forced-no', ['only' => ['login','register']]);
	}


	public function login(Request $request)
	{
			$validator = Validator::make(Input::all(), [
				'email' 	=> 'required|email',
				'password' 	=> 'required|string|min:6|max:100',
			]);
			if($validator->fails())
				return $this->error(400, $validator->messages());

		// Email o Pass en uso?

			$credentials = Input::only('email', 'password');
			if(!Auth::validate($credentials))
				return $this->error(400, 'Wrong mail/password. Please re-enter.');

		// Logueamos

			$user 	= User::whereEmail(Input::get('email'))->first();
			$token	= JWTAuth::fromUser($user);

		// Preparamos resultado

			Auth::login($user);
			$user->is_admin	= Auth::isAdmin();
			$user->skill 	= $user->skill_info ? $user->skill_info->name : '';

			$user->clean();

			$data = [	'user'	=> $user,
						'token'	=> $token ];


		return $this->success($data);
	}

	public function register(Request $request, Registrar $registrar)
	{
			$validator = Validator::make(Input::all(), [
				'name'			=> 'string|min:6',
				'email' 		=> 'required|email',
				'password' 		=> 'required|string|min:6',
				'country_id' 	=> 'required|integer|min:1',
			]);
			if($validator->fails())
				return $this->error(400, $validator->messages());

		// Mail en uso?

			if (User::whereEmail(Input::get('email'))->count())
				return $this->error(400, 'Email in use.');

		// Le registramos

			$registrar->create(Input::only('name', 'email', 'password', 'country_id'));

		// Login

			$user 	= User::whereEmail(Input::get('email'))->first();
			$user->country_id = Input::get('country_id');
			$user->save();
			$token	= JWTAuth::fromUser($user);

		// Preparamos

			Auth::login($user);
			$user->is_admin	= Auth::isAdmin();
			$user->skill 	= $user->skill_info ? $user->skill_info->name : '';

			$user->clean();

			$data = [	'user'	=> $user,
						'token'	=> $token ];
			
		// Enviamos email a wevote

			$user->country;

			Mail::send('emails.new_register', ['user' => $user], function($message)
			{
				$message->to('wevote@worldsurfteam.net', 'World Surf Team')->subject('User registered!');
			});

			Mail::send('emails.welcome', ['user' => $user], function($message) use ($user)
			{
				$message->to($user->email, '')->subject('Welcome to WorldSurfTeam.net!');
			});



		return $this->success($data);
	}

	public function forgot_password_send_link(Request $request)
	{
			$validator = Validator::make(Input::all(), [
				'email' 		=> 'required|email',
			]);
			if($validator->fails())
				return $this->error(400, $validator->messages());

			$response = $this->passwords->sendResetLink(Input::only('email'), function($m)
			{
				$m->subject('Your password reset link');
			});

			switch ($response)
			{
				case PasswordBroker::RESET_LINK_SENT:
					return $this->success('Email sent.');

				case PasswordBroker::INVALID_USER:
					return $this->error(400, trans($response));
				default:
			}

		return $this->error(400, 'Unknown error');
	}

	public function forgot_password_apply(Request $request)
	{
		$validator = Validator::make(Input::all(), [
			'token' 	=> 'required',
			'email' 	=> 'required',
			'password' 	=> 'required|confirmed',
		]);
		if($validator->fails())
			return $this->error(400, $validator->messages());

		$credentials = $request->only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = $this->passwords->reset($credentials, function($user, $password)
		{
			$user->password = bcrypt($password);
			$user->save();
		});

		switch ($response)
		{
			case PasswordBroker::PASSWORD_RESET:
				return $this->success();
			default:
				return $this->error(trans($response));
		}

		return $this->success();
	}

	public function check(Request $request)
	{
		// Check not user

			if(is_null(Auth::user()))
				return $this->success(false);

		// Prepare

			$user 			= Auth::user();
			$user->is_admin	= Auth::isAdmin();
			$user->skill 	= $user->skill_info ? $user->skill_info->name : '';

			$user->clean();


		return $this->success($user);
	}

	public function get_top_voted_surfers(Request $request)
	{
		// Prepare

			$user 			= Auth::user();
			if(!is_null($user))
				$user_id = $user->id;
			else
				$user_id = 1;

		// Sacamos los puntos de cada surfer

			$votes = DB::table('votes')->leftJoin('waves', 'waves.id', '=', 'votes.wave_id')->whereUserId($user_id)->get();
			$points 	= [];
			foreach($votes as $vote)
			{
				if(!isset($points[$vote->surfer_id]))
					$points[$vote->surfer_id] = 0;

				$points[$vote->surfer_id] += $vote->points;
			}
			$surfersId = array_keys($points);

		// Obtenemos info de todos los surfers

			$allSurfers = Surfer::whereIn('id', $surfersId)->get();
			$surfersList = [];
			foreach($allSurfers as $surfer)
			{
				$surfer->country;
				$surfersList[$surfer->id] = $surfer;
			}

		// Preparamos resultado

			$result = [];
			$results_limit = 10;
			foreach($points as $surferId => $point)
			{
				$results_limit--;
				if($results_limit < 0)
					break;

				$surfer 		= $surfersList[$surferId];
				$surfer->points = $point;
				$result[] = $surfer;
			}


		return $this->success($result);
	}


}
