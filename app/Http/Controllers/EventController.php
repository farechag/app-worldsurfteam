<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;

use Validator;
use Input;
use Auth;
use DB;

use App\Country;
use App\Event;
use App\Surfer;
use App\Round;
use App\Heat;
use App\EventType;
use App\EventSponsor;
use App\EventParticipation;

class EventController extends ApiController {



		public function __construct(Output $output)
		{
			parent::__construct($output);

			$this->middleware('auth-only-admin', ['except' => ['get_all', 'get_events', 'get_heats', 'get_surfers', 'get_types', 'get_sponsors']]);
			$this->middleware('check-input');
		}

		public function new_event(Request $request)
		{
				$validator = Validator::make(Input::all(), [
					'name'			=> 'required|string',
					'location'		=> 'required|string',
					'start_date'	=> 'required|integer|min:1',
					'finish_date'	=> 'required|integer|min:1',
					'country_id'	=> 'integer|min:1',
					'type_id'		=> 'integer|min:1',
					'sponsor_id'	=> 'integer|min:1',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Cargamos la info

				$data = Input::only('name','location','start_date','finish_date','country_id', 'sponsor_id', 'type_id');

			// Ponemos la fecha HR

				$data['start_date_human'] 	= date('d-m-Y',$data['start_date']);
				$data['finish_date_human'] 	= date('d-m-Y',$data['finish_date']);

			// Guardamos

				$event = Event::create($data);

			// Añadimos los surfers (v0.12)

				$surfersWSL = surfer::whereWsl(true)->select('id')->get();
				$participations = [];
				foreach($surfersWSL as $i => $surfer)
				{
					$participations[] = [	'event_id'	=> $event->id,
											'surfer_id'	=> $surfer->id ];
				}
				DB::table('events_participations')->insert($participations);


			return $this->success();
		}

		public function add_surfer(Request $request, $event_id, $surfer_id)
		{
				if(is_null(Event::find($event_id)))
					return $this->error(404, 'Event not found.');
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');

			// Ya estará asociado al evento?

				$exists = EventParticipation::where([	'event_id'	=> $event_id,
														'surfer_id'	=> $surfer_id])
														->count();
				if($exists)
					return $this->error(403, 'Surfer already associated with this event.');

			// Le añadimos

				EventParticipation::create([	'event_id'	=> $event_id,
												'surfer_id'	=> $surfer_id]);


			return $this->success();
		}

		public function del_surfer(Request $request, $event_id, $surfer_id)
		{
				if(is_null(Event::find($event_id)))
					return $this->error(404, 'Event not found.');
				if(is_null(Surfer::find($surfer_id)))
					return $this->error(404, 'Surfer not found.');

			// Igual no está asociado al evento

				$exists = EventParticipation::where([	'event_id'	=> $event_id,
														'surfer_id'	=> $surfer_id])
														->count();
				if(!$exists)
					return $this->error(403, 'Surfer not associated with this event.');

			// Le quitamos

				EventParticipation::where([	'event_id'	=> $event_id,
											'surfer_id'	=> $surfer_id])
											->delete();


			return $this->success();
		}

		public function set_rounds(Request $request, $event_id)
		{
				if(is_null(Event::find($event_id)))
					return $this->error(404, 'Event not found.');
				$validator = Validator::make(Input::all(), [
					'rounds_count'	=> 'required|integer|min:1|max:20',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Comprobamos que no tenga rondas creadas

				$event 	= Event::find($event_id);
				if($event->rounds->count())
					return $this->error(403, 'This event has already created the rounds.');

			// Preparamos el array de rondas

				$rounds = Input::get('rounds_count');
				$data = [];
				for($i=0;$i<$rounds;$i++)
				{
					$number = $rounds - $i;
					switch($i)
					{
						case 0:	$number = 'Final'; break;
						case 1: $number = 'Semis'; break;
						case 2: $number = 'Quarters'; break;
					}
					$data[] = [	'event_id'	=> (int) $event_id,
								'number'	=> $number ];
				}
				$rounds = \array_reverse($data);

			// Las guardamos

				// Round::create($rounds);
				DB::table('rounds')->insert($rounds);


			return $this->success();
		}

		public function get_all(Request $request)
		{
			// Cargamos la lista de eventos

				$events = Event::all();

			// Preparamos atributos

				foreach($events as $event)
				{
					$event->active();
					$event->country;
					$event->type;
					$event->sponsor;
				}

			// Caché ETAG

				$headers 	= getallheaders();
				$oldEtag	= isset($headers['If-None-Match']) ? $headers['If-None-Match'] : false;
				$newEtag 	= md5(json_encode($events));
				if($newEtag == $oldEtag)
					return $this->notModified();


			return $this->success($events, null, ['ETag'=> $newEtag]);
		}

		public function get_event(Request $request, $event_id)
		{
			// Obtenemos el evento

				$event		= Event::find($event_id);

			// Preparamos resultado

				if(is_null($event))
					return $this->error(404, 'Event not found.');

			// Obtenemos atributos

				$event->active();
				$event->type;
				$event->sponsor;


			return $this->success($event);
		}

		public function get_heats(Request $request, $event_id)
		{
				if(is_null(Event::find($event_id)))
					return $this->error(404, 'Event not found.');

			// Obtenemos la lista de rondas

				$rounds 	= Event::find($event_id)->rounds;

			// Preparamos el resultado en forma de round_id -> heats

				$result		= [];
				foreach($rounds as $round)
				{
					$round->heats;
					foreach($round->heats as $heat)
					{
						$heat->participations;
						foreach($heat->participations as $part)
						{
							$part->surfer;
							$part->surfer->country;
						}
						$heat->status();
						$heat->active();
						$heat->round;
					}
					$result[$round->id] = $round;
				}

			// Caché ETAG

				$headers 	= getallheaders();
				$oldEtag	= isset($headers['If-None-Match']) ? $headers['If-None-Match'] : false;
				$newEtag 	= md5(json_encode($result));
				if($newEtag == $oldEtag)
					return $this->notModified();


			return $this->success($result, null, ['ETag'=> $newEtag]);
		}

		public function get_surfers(Request $request, $event_id)
		{
				if(is_null(Event::find($event_id)))
					return $this->error(404, 'Event not found.');

			// Obtenemos la lista de surfers

				// Version 0.1
				$surfers 	= Event::find($event_id)->surfers;

				// Version 0.11 CANCELADO, se ordenarán por apellido (para poder ordenarlos según su participation)
					// $surfers 		= Event::find($event_id)->surfers;
					// $participations = Event::find($event_id)->participations->lists('id', 'surfer_id');
					//
					// foreach($surfers as $i=>$surfer)
					// {
					// 	$surfer->event_participation_id = $participations[$surfer->id];
					// }


			return $this->success($surfers);
		}

		public function get_types (Request $request)
		{
			$types = EventType::all();

			return $types;
		}

		public function get_sponsors (Request $request)
		{
			$sponsors = EventSponsor::all();

			return $sponsors;
		}




}
