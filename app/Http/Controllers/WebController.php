<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use App;

class WebController extends Controller {





	public function __construct()
	{
		// Para eliminar las cookies de sesión de laravel
		\Config::set('session.driver', 'array');

		//$this->middleware('auth');
		//$this->middleware('auth-only-admin', 	['only' => ['admin']]);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function client()
	{
		Blade::setContentTags('<%', '%>');        // for variables and all things Blade
    	Blade::setEscapedContentTags('<%%', '%%>');
		return View('client', ['env' => App::environment()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function admin()
	{
		Blade::setContentTags('<%', '%>');        // for variables and all things Blade
    	Blade::setEscapedContentTags('<%%', '%%>');
		return View('admin', ['env' => App::environment()]);
	}


}
