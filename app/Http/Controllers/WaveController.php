<?php namespace App\Http\Controllers;

use App\Http\Responses\Output;
use Illuminate\Http\Request;

use Validator;
use Input;
use Hash;
use Auth;
use DB;

use	App\User;
use App\Event;
use App\Surfer;
use App\Heat;
use App\Round;
use App\Vote;
use App\Wave;
use App\EventParticipation;
use App\HeatParticipation;

class WaveController extends ApiController {



		public function __construct(Output $output)
		{
			parent::__construct($output);

			$this->middleware('auth-forced', 		['only' => ['vote']]);
			$this->middleware('auth-only-admin', 	['except' => ['vote']]);
			$this->middleware('check-input');
		}

		public function vote(Request $request, $wave_id)
		{
				$validator = Validator::make(Input::all(), [
					'points'	=> 'required|numeric|min:0|max:10',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Comprobemos la ola

				$wave = Wave::find($wave_id);
				if(is_null($wave))
					return $this->error(404, 'Wave not found.');
				if(!$wave->active())
					return $this->error(403, 'The wave is over.');

			// Veamos si la he votado ya

				$user = Auth::user();
				if(Vote::where([ 'user_id'	=> $user->id,
								 'wave_id'	=> $wave->id])->count())
					return $this->error(403, 'Wave already voted.');

			// La votamos

				$points = Input::get('points');
				$points = round($points*100)/100;
				Vote::create([ 	'user_id'	=> $user->id,
								'wave_id'	=> $wave->id,
								'points'	=> $points,
								'date'		=> time()]);


			return $this->success();
		}

		public function finish_wave(Request $request, $wave_id)
		{
			// Comprobemos la ola

				$wave = Wave::find($wave_id);
				if(is_null($wave))
					return $this->error(404, 'Wave not found.');
				if(!$wave->active())
					return $this->error(403, 'The wave is over.');

			// La finalizamos

				unset($wave->active);
				$wave->finish_date = time();
				$wave->save();


			return $this->success();
		}


		public function new_wave(Request $request)
		{
				$validator = Validator::make(Input::all(), [
					'heat_id' 		=> 'required|numeric',
					'surfer_id'		=> 'required|numeric',
					'duration'		=> 'numeric',
				]);
				if($validator->fails())
					return $this->error(400, $validator->messages());

			// Veamos cuantas olas tiene ya activas

				$active_waves = Wave::onlyActives()->where(Input::only('heat_id','surfer_id'))->count();
				if($active_waves > 1)
					return $this->error(400, 'There are already two waves for this surfer.');

			// Obtenemos el número que le toca

				$waves = Wave::where(Input::only('heat_id', 'surfer_id'))->count();
				$number = $waves+1;

			// Creamos los datos

				$wave = Input::only('heat_id','surfer_id');
				$wave['start_date']		= time();
				$wave['finish_date'] 	= time() + Input::get('duration', 180);
				$wave['number']			= $number;

			// Guardamos

				Wave::create($wave);


			return $this->success();
		}

		public function del_wave(Request $request, $wave_id)
		{
			// Obtenemos la ola

				$wave = Wave::find($wave_id);

			// Comprobemos que existe

				if(is_null($wave))
					return $this->error(404, "wave not found.");

			// La borramos

				$wave->delete();


			return $this->success();
		}

}
