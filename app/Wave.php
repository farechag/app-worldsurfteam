<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Vote;

class Wave extends Model {

    protected $table    = 'waves';
    public $timestamps  = false;
    protected $fillable = ['heat_id', 'surfer_id', 'start_date', 'finish_date', 'number'];

    // Relationships

        public function heat()
        {
            return $this->belongsTo('App\Heat', 'heat_id', 'id');
        }

        public function surfer()
        {
            return $this->hasOne('App\Surfer', 'id', 'surfer_id');
        }

        public function votes()
        {
            return $this->hasMany('App\Vote', 'wave_id', 'id');
        }


    // Attributes

        // Consideramos activo si ha pasado su fecha de start pero no la de finish
        public function active()
        {
            $restante = $this->finish_date - time();
            $elapsed  = time() - $this->start_date;

            $finished   = $restante < 0;
            $not_started= $elapsed < 0;

            $this->active  = ! ($finished || $not_started);

            return $this->active;
        }


    // Scopes

        // Consideraremos activas aquellas que su fecha de finish aun no haya pasado
        public function scopeOnlyActives($query)
        {
            return $query->where('finish_date','>',time());
        }

        public function scopeOnlyFinished($query)
        {
            return $query->where('finish_date','<',time());
        }

        // Devuelve las olas que no tengan score aun
        public function scopeOnlyWithoutScore($query)
        {
            return $query->whereNull('score');
        }


    // Methods

        // Calcula los puntos de la Wave elegida (o de varias)
        // Ej: Wave::find(1)->updateScore()
        // Ej: Wave::updateScore();
        public function scopeUpdateScore($query)
        {
            $count = $query->count();
            $query->get()->each(function($wave)
            {
                $points_array = Vote::where('wave_id', $wave->id)->lists('points');
                sort($points_array);

                $all    = count($points_array);
                $score  = 0;
                if($all > 0)
                {
                    $valid_points = $points_array;
                    if($all >= 5)
                    {
                        $extrems= round($all*0.2);
                        $main   = $all - 2*$extrems;
                        if($main < 0)
                            $main = 0;

                        $valid_points   = array_slice($points_array, $extrems, $main);
                    }

                    $score = array_sum($valid_points)/count($valid_points);
                }

                $wave->score = $score;
                $wave->votes_count = $all;
                $wave->save();
            });
            return $count+1;
        }

}
