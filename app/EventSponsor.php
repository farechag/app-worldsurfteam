<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSponsor extends Model {

    protected $table = 'events_sponsors';
    public $timestamps = false;

    public function events()
    {
        return $this->hasMany('App\Event', 'sponsor_id', 'id');
    }

}
