<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Heat extends Model {

    protected $table = 'heats';
    public $timestamps = false;
    protected $fillable = ['round_id', 'start_date', 'finish_date', 'number'];
    protected $primaryKey = 'id';

    // Relationships

        public function round()
        {
            return $this->belongsTo('App\Round', 'round_id', 'id');
        }

        public function waves()
        {
            return $this->hasMany('App\Wave', 'heat_id', 'id');
        }

        public function participations()
        {
            return $this->hasMany('App\HeatParticipation', 'heat_id', 'id');
        }

        public function surfers()
        {
            return $this->belongsToMany('App\Surfer', 'heats_participations', 'heat_id', 'surfer_id');
        }


    // Attributes

        // Consideramos activo si tiene fecha de inicio pero no de fin, o si los tiene, si al menos aun estamos en ese rango
		public function active()
		{
            if($this->status() == 'active')
                $this->active = true;
            else
                $this->active = false;

            return $this->active;
		}

        // not_started, active, finished
        public function status()
        {
            $has_start  = !is_null($this->start_date);
            $has_finish = !is_null($this->finish_date);
            $finished   = $this->finished;

            $status = 'not_started';
            if($finished)
            {
                $status = 'finished';
            }else{
                if($has_start)
                {
                    $status = 'active';
                }else{
                    $status = 'not_started';
                }
            }

            $this->status = $status;

            return $this->status;
        }

    // Scopes

        public function scopeOnlyFinished ($query)
        {
            return $query->whereFinished(true);
        }

        // Devuelve aquellos heats que tienen alguna participación sin Score
        public function scopeOnlyNotFullCalculated ($query)
        {
            return $query->whereHas('participations', function($q)
            {
                $q->whereNull('score_wst');
            });
        }


    // Methods

        // Calcula los puntos del heat elegido (o de varios)
        // Ej: Wave::find(1)->updatePoints()
        // Ej: Wave::updatePoints();
        public function scopeUpdateScore($query)
        {
            $count = $query->count();
            $query->get()->each(function($heat)
            {
                HeatParticipation::where('heat_id','=',$heat->id)
                               ->updateScore();
            });
            return $count+1;
        }



/*


    // Actualiza el Score de una Wave o todas
    public function scopeUpdateScore()
    {
        $this->GetUpdateable()->get()->each(function($heat)
        {
            $heat->participations()->GetUpdateable()->get()->each(function($participation)
            {
                $participation_score        = $participation->getScore() -1;
                DB::table('heats_participations')->where('id','=',$participation->id)
                    ->update(['score_wst' => $participation_score]);
            });
        });
    }
*/
}
