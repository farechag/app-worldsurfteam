<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model {

    protected $table = 'users_skills';
    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\User', 'skill_id', 'id');
    }
}
