<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model {

    protected $table = 'rounds';
    public $timestamps = false;
    protected $fillable = ['event_id', 'number'];
    protected $casts = [
	    'id' => 'integer',
	];

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function heats()
    {
        return $this->hasMany('App\Heat', 'round_id', 'id');
    }

}
