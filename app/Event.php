<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	protected $table 	= 'events';
	public $timestamps 	= false;
	protected $guarded = [];

	// Relationships

		public function type()
		{
			return $this->belongsTo('App\EventType', 'type_id', 'id');
		}

		public function sponsor()
		{
			return $this->belongsTo('App\EventSponsor', 'sponsor_id', 'id');
		}

		public function country()
		{
			return $this->belongsTo('App\Country', 'country_id', 'id');
		}

		public function rounds()
		{
			return $this->hasMany('App\Round', 'event_id', 'id');
		}

		public function participations()
		{
			return $this->hasMany('App\EventParticipation', 'event_id', 'id');
		}

		public function surfers()
		{
			return $this->belongsToMany('App\Surfer', 'events_participations', 'event_id', 'surfer_id');
		}


	// Attributes

        // Consideramos activo si ha pasado su fecha de start pero no la de finish
		public function active()
		{
            $restante = $this->finish_date - time();
            $elapsed  = time() - $this->start_date;

            $finished   = $restante < 0;
            $not_started= $elapsed < 0;

			$this->active  = ! ($finished || $not_started);

			return $this->active;
		}


    // Methods




}
