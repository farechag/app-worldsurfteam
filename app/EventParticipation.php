<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventParticipation extends Model {

    protected $table = 'events_participations';
    public $timestamps = false;
    protected $fillable = array('surfer_id', 'event_id');

    // Relationships

        public function event()
        {
            return $this->belongsTo('App\Event', 'event_id', 'id');
        }

        public function surfer()
        {
            return $this->belongsTo('App\Surfer', 'surfer_id', 'id');
        }


}
