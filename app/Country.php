<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table    = 'countries';
    public $timestamps  = false;

    // Relationships
    
        public function events()
        {
            return $this->hasMany('App\Event', 'country_id', 'id');
        }

        public function surfers()
        {
            return $this->hasMany('App\Surfer', 'country_id', 'id');
        }

        public function users()
        {
            return $this->hasMany('App\Surfer', 'country_id', 'id');
        }
}
