<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HeatParticipation extends Model {

    protected $table = 'heats_participations';
    public $timestamps = false;
    protected $fillable = array('surfer_id', 'heat_id');
    protected $primaryKey = "id";

    public function heat()
    {
        return $this->belongsTo('App\Heat', 'heat_id', 'id');
    }

    public function surfer()
    {
        return $this->belongsTo('App\Surfer', 'surfer_id', 'id');
    }

    // Calcula los puntos de la participación elegida (o de varias)
    // Primero llama a Wave::updateScore de las olas que va a usar.
    // Ej: HeatParticipation::find(1)->updateScore()
    // Ej: HeatParticipation::updateScore();
    public function scopeUpdateScore($query)
    {
        $query->get()->each(function($participation)
        {
            $surfer_id  = $participation->surfer_id;
            $heat_id    = $participation->heat_id;

            // Updateamos los puntos de las olas antes de usarlos

            Wave::where([   'surfer_id' => $surfer_id,
                            'heat_id'   => $heat_id])->updateScore();

            $scores      = Wave::where([    'surfer_id' => $surfer_id,
                                            'heat_id'   => $heat_id])
                                ->whereNotNull('score')->orderBy('score','desc')->take(2)->lists('score');
            if(count($scores))
                $score = array_sum($scores);
            else
                $score = 0;

            $participation->score_wst = $score;
            $participation->save();
        });
    }
}
