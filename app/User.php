<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Riogo\Permiso\UserRolesTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	use UserRolesTrait;

	protected $table 	= 'users';
	protected $fillable = ['name', 'email', 'password', 'country_id'];
	protected $hidden 	= ['password', 'remember_token'];
	protected $casts = [
	    'country_id' => 'integer',
	];

	// Relationships

		public function country()
		{
			return $this->belongsTo('App\Country', 'country_id', 'id');
		}

		public function skill_info()
		{
			return $this->belongsTo('App\UserSkill', 'skill_id', 'id');
		}

		public function votes()
		{
			return $this->hasMany('App\Vote', 'user_id', 'id');
		}


	// Methods

		public function clean()
		{
			unset($this->skill_info);
			unset($this->roles);
			unset($this->skill_id);
			unset($this->created_at);
			unset($this->updated_at);
		}


}
