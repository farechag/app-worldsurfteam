<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

    protected $table = 'votes';
    public $timestamps = false;
    protected $fillable = ['user_id', 'wave_id', 'points', 'date'];


    public function wave()
    {
        return $this->belongsTo('App\Wave', 'wave_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
