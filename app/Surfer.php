<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Surfer extends Model {

    protected $table = 'surfers';
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
	    'wsl' => 'boolean',
	];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function eventParticipations()
    {
        return $this->hasMany('App\EventParticipation', 'surfer_id', 'id');
    }
    public function heatParticipations()
    {
        return $this->hasMany('App\HeatParticipation', 'surfer_id', 'id');
    }

    public function waves()
    {
        return $this->hasMany('App\Wave', 'surfer_id', 'id');
    }


    public function scopeGetEvents($query, $surfer_id)
    {
        $events_ids     = Array();
        $participations = EventParticipation::where('surfer_id','=',$surfer_id)->get();
        foreach($participations as $participation)
            $events_ids[] = $participation['event_id'];

        $events = Event::whereIn('id',$events_ids)->get();

        return $events;
    }

}
