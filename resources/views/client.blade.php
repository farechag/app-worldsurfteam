<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>World Surf Team</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="icon" type="image/x-icon" href="http://app.worldsurfteam.net/favicon.ico">
		<link rel="apple-touch-icon" href="http://app.worldsurfteam.net/apple-touch-icon.png">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="static-client/css/main.css">
		<link rel="stylesheet" href="static-client/css/animate.css">
		<link rel="stylesheet" href="static-client/css/animations.css">
		<!-- <link rel="stylesheet" href="css/responsiveslides.css"> -->

		<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="https://code.angularjs.org/1.3.14/angular.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-animate.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular-route.min.js"></script>
        <!--<script src="js/vendor/angular-animate.min.js.map"></script>-->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular-animate.min.js.map"></script>-->

	</head>
	<body ng-app="wst" ng-controller="mainCtrl as mv" ng-init="mv.init()">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->



	<div id="container_god" class="container-fluid">
		<div id="wrapper-view-father" class="row">
			<div id="wrapper-view" class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-5 col-sm-offset-4 col-lg-4 col-lg-offset-4">
				<div id="view-element" class="" ng-view>

				</div><!--
			--><div id="menu_page" class="page-view">
				    <div class="columna">
					    <div class="wrapper">
					        <div class="logo-small"></div>
					        <div class="menu-3-lines-white pull-right clickable" ng-click="closeMenu()"></div>
					        <div class="menu-wrapper">
								<a class="half-bg-opacity" ng-href="#/login" ng-if="!user.data">Sign in</a>
								<a class="half-bg-opacity clickable" ng-click="user.logout()" ng-if="user.data">Logout</a>
								<a ng-href="#/">Dashboard</a>
					        </div>
					    </div>
				    </div>
					<div class="bg-closer" ng-click="closeMenu()"></div>
				</div>
			</div>
		</div>
	</div>




	<div class="modal fade" id="modal_errors" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">New message</h4>
		  </div>
		  <div class="modal-body">

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>


	<!-- *********************************************************************************** -->


		<!-- Environment -->
		<script charset="utf-8">
			var server_env = '<% $env %>';
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

		<script src="js/vendor/bootstrap.min.js"></script>
		<!-- <script src="js/vendor/routie.min.js"></script> -->
		<script src="js/vendor/countdown.min.js"></script>
		<!--<script src="js/vendor/responsiveslides.min.js"></script>-->
		<!-- <script src="js/vendor/md5.js"></script>
		<script src="js/vendor/utf8_encode.js"></script>
	-->
		<script src="js/vendor/moment-with-locales.min.js"></script>
		<script src="js/vendor/bootstrap-datepicker.js"></script>
		<!-- <script src="js/vendor/jquery.marquee.min.js"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jStorage/0.4.12/jstorage.min.js"></script>
		<script src="http://rawgithub.com/cchantep/bootstrap-combobox/master/js/bootstrap-combobox.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>

		<!--
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.6.0/ui-bootstrap-tpls.min.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/datePicker.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/datePickerUtils.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/dateRange.js"></script>

-->
		<script src="js/general.js"></script>




		<script src="static-client/js/ready.js"></script>
		<script src="static-client/js/app.js"></script>
		<script src="static-client/js/routes.js"></script>
		<script src="static-client/js/filters.js"></script>

        <!-- Models -->
		<script src="static-client/js/models/user.js"></script>
		<script src="static-client/js/models/event.js"></script>
		<script src="static-client/js/models/heat.js"></script>
		<script src="static-client/js/models/wave.js"></script>
		<script src="static-client/js/models/surfer.js"></script>

		<!-- controllers -->
		<script src="static-client/js/mainCtrl.js"></script>

		<!-- Google Analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-59059722-1', 'auto');
			//ga('send', 'pageview', 'app');

		</script>




	</body>
 </html>
