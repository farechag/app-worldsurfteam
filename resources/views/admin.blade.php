<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Backoffice | World Surf Team</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="http://app.worldsurfteam.net/favicon.ico">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/datepicker3.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="static-admin/css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="https://code.angularjs.org/1.3.9/angular.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular-route.min.js"></script>


    </head>
    <body ng-app="wst" ng-controller="mainCtrl as vm" ng-init="vm.init()">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->



    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#/">Backoffice <small style="opacity:0.5;">[v0.13]</small></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form id="form-login" class="navbar-form navbar-right enter-click-parent" role="form"  ng-if="!user.logued()">
            <div class="form-group">
              <input name="email" type="text" placeholder="Email" class="form-control" ng-model="vm.formLogin.email">
            </div>
            <div class="form-group">
              <input name="password" type="password" placeholder="Password" class="form-control" ng-model="vm.formLogin.password">
            </div>
            <button type="button" class="btn btn-success" ng-click="user.login(vm.formLogin)">Login</button>
          </form>
			<form class="navbar-form navbar-right enter-click-parent" role="form"  ng-if="user.logued()">
				<button type="button" class="btn btn-success" ng-click="user.logout()">Logout</button>
			  </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid" ng-if="user.admin()">

		<div ng-view class="hidden"></div>
        <div id="selector" class="row">
            <div id="events" class="col-xs-5">
                <h3>Events</h3>
				<div class="wrapper">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>name</th>
								<th>Start (UTC)</th>
								<th>Finish (UTC)</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="e in event.all | object2Array | orderBy:'start_date'" ng-class="{warning: e.id == event.currentId}">
								<td>{{ e.id }}</td>
								<td>{{ e.name }}</td>
								<td>{{ e.start_date | unixToString}}</td>
								<td>{{ e.finish_date | unixToString}}</td>
								<td><a ng-href="admin#/event/{{e.id}}">Go</a></td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
            <div id="rounds" class="col-xs-2">
                <h3>Rounds</h3>
				<div class="wrapper">
					<table class="table table-striped table-hover rounds-table" ng-class="{hidden: !(currentEventRounds | length) && event.currentId}">
						<thead>
							<tr>
								<th>ID</th>
								<th>Number</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="round in event.currentRounds | object2Array | orderBy:'id'" ng-class="{warning: round.id == event.currentRoundId}">
								<td>{{ round.id }}</td>
								<td>{{ round.number }}</td>
								<td><a ng-href="admin#/event/{{event.currentId}}/round/{{round.id}}">Go</a></td>
							</tr>
						</tbody>
					</table>
                    <div style="width:160px;" ng-if="!(event.currentRounds | length) && event.currentId">
                        <p>This event has no rounds. Create them now:</p>
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="# of rounds" ng-model="formEventCreateRounds">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button" ng-click="event.createRounds(formEventCreateRounds)">Send!</button>
                          </span>
                        </div><!-- /input-group -->
                    </div>
				</div>
            </div>
			<div id="heats" class="col-xs-5">
				<h3>Heats</h3>
				<table class="table table-striped table-hover rounds-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Number</th>
							<th>Surfers</th>
							<th>Status</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="h in event.currentHeats" ng-class="{warning: h.id == heat.id}" ng-init="startHeatDuration = []">
							<td>{{ h.id }}</td>
							<td>{{ h.number }}</td>
							<td><span ng-repeat="p in h.participations">{{surfer.get(p.surfer_id).nickname}}<span ng-if="!$last"> | </span></span></td>
							<td>{{ h.status }}</td>
							<td>
                                <span ng-if="h.status == 'not_started'" ng-init="startHeatDuration[h.id] = 30">
                                    Duration (min): <input type="numeric" ng-model="startHeatDuration[h.id]"/>
                                </span>

                            </td>
                            <td ng-if="h.status == 'not_started'"><button class="btn btn-default" type="button" ng-click="heat.updateStatus(h.id, 'on', startHeatDuration[h.id])">Start!</button></td>
							<td ng-if="h.status == 'active'"><a ng-href="admin#/event/{{event.currentId}}/round/{{event.currentRoundId}}/heat/{{h.id}}">Go</a></td>
							<td ng-if="h.status == 'finished'"><a ng-href="admin#/event/{{event.currentId}}/round/{{event.currentRoundId}}/heat/{{h.id}}">See Results</a></td>
						</tr>
					</tbody>
				</table>
            </div>
        </div>
        <hr size=2>

		<div id="event_detail" class="row detailed-info" ng-if="root=='event'">
			<h2>Event <small>{{ event.current.name }}</small></h2>
			<div class="col-xs-6">
				<h3>Surfers WSL ( {{ event.currentSurfers | onlySurfersWSL | length }} )
					<div class="dropdown dropdown-surfers">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						Add
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation" ng-repeat="surfer in surfer.all | onlySurfersWSL | notIn:event.currentSurfers | object2Array | orderBy:['lastname', 'name']"><a role="menuitem" tabindex="-1" ng-click="event.addSurfer(surfer.id)">{{surfer.name}} {{surfer.lastname}}</a></li>
					  </ul>
					</div>
				</h3>
				<table class="table table-striped table-hover surfers-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>fullname</th>
							<th>country</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="surfer in event.currentSurfers | onlySurfersWSL | object2Array | orderBy:['lastname', 'name']">
							<td>{{ surfer.id }}</td>
							<td>{{ surfer.name }} {{ surfer.lastname }}</td>
							<td>{{ $root.countries[surfer.country_id].name }}</td>
							<td><button class="btn btn-link" type="submit" ng-click="event.delSurfer(surfer.id)">Delete</button></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-xs-6">
				<h3>Surfers Wildcard ( {{ event.currentSurfers | onlySurfersNotWSL | length }} )
					<div class="dropdown dropdown-surfers">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						Add
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation" ng-repeat="surfer in surfer.all | onlySurfersNotWSL | notIn:event.currentSurfers | object2Array | orderBy:['lastname', 'name']"><a role="menuitem" tabindex="-1" ng-click="event.addSurfer(surfer.id)">{{surfer.name}} {{surfer.lastname}}</a></li>
					  </ul>
					</div>
				</h3>
				<table class="table table-striped table-hover surfers-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>fullname</th>
							<th>country</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="surfer in event.currentSurfers | onlySurfersNotWSL | object2Array | orderBy:['lastname', 'name']">
							<td>{{ surfer.id }}</td>
							<td>{{ surfer.name }} {{ surfer.lastname }}</td>
							<td>{{ $root.countries[surfer.country_id].name }}</td>
							<td><button class="btn btn-link" type="submit" ng-click="event.delSurfer(surfer.id)">Delete</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row detailed-info" ng-if="root=='round'">
			<h2>Round {{ event.currentRounds[event.currentRoundId].number }} <small>#{{ event.currentRoundId }}</small></h2>
			<div class="col-xs-6">
				<h3>Heats <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_create_heat">add</button></h3>
                <table class="table table-striped table-hover rounds-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Surfers</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="h in event.currentHeats" ng-class="{success: h.id == heat.id}">
                            <td>{{ h.id }}</td>
                            <td><span ng-repeat="p in h.participations">[{{surfer.get(p.surfer_id).name}} {{surfer.get(p.surfer_id).lastname}}] </span></td>
                            <td>{{ h.status }}</td>
                            <td><button class="btn btn-link" type="submit" ng-click="event.delHeat(h.id)">Delete</button></td>
                        </tr>
                    </tbody>
                </table>
			</div>
		</div>


        <!-- New Event -->
		<div class="row detailed-info" ng-if="root=='/'" >

            <div class="col-xs-6">
                <h2>Create Event</h2>
                <br>
                <form class="form-horizontal" ng-init="newEvent = {start_date:'2015-04-03 07:00:00', finish_date:'2015-05-03 07:00:00'}">
                    <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                    <input style="display:none" type="text" name="fakeusernameremembered"/>
                    <input style="display:none" type="password" name="fakepasswordremembered"/>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" ng-model="newEvent.name" class="form-control" id="inputName" placeholder="Super evento de surf">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputLocation" class="col-sm-2 control-label">Location</label>
                    <div class="col-sm-10">
                      <input type="text" ng-model="newEvent.location" class="form-control" id="inputLocation" placeholder="Valencia">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputStart" class="col-sm-2 control-label">Start date (UTC)</label>
                    <div class="col-sm-10">
                      <input type="text" ng-model="newEvent.start_date" class="form-control" id="inputStart" placeholder="2015-04-03 07:00:00">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputFinish" class="col-sm-2 control-label">Finish date (UTC)</label>
                    <div class="col-sm-10">
                      <input type="text" ng-model="newEvent.finish_date" class="form-control" id="inputFinish" value="2015-05-03 07:00:00" placeholder="2015-05-03 07:00:00">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputCountry" class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-10">
                      <select ng-model="newEvent.country_id">
                          <option ng-repeat="c in countries | object2Array | orderBy:'name'" value="{{ c.id }}">{{ c.name }}</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputType" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-10">
                      <select ng-model="newEvent.type_id">
                          <option ng-repeat="c in eventTypes" value="{{ c.id }}">{{ c.name }}</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputSponsor" class="col-sm-2 control-label">Sponsor</label>
                    <div class="col-sm-10">
                      <select ng-model="newEvent.sponsor_id">
                          <option ng-repeat="c in eventSponsors" value="{{ c.id }}">{{ c.name }}</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-default" ng-click="event.new(newEvent)" ng-disabled="(newEvent | object2Array | length) < 1">Apply</button>
                    </div>
                  </div>
                </form>



            </div>
            <div class="col-xs-6">
				<h2>All Surfers</h2>
				<table class="table table-striped table-hover surfers-table" ng-init="$root.newAllSurfers = {}">
					<thead>
						<tr>
							<th>ID</th>
							<th>name</th>
							<th>lastname</th>
							<th>nickname</th>
							<th>country</th>
							<th>is_wsl?</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr class="info">
							<td>ADD</td>
							<td><input type="text" class="form-control" placeholder="Enter name" ng-model="$root.newAllSurfers.name"></td>
							<td><input type="text" class="form-control" placeholder="Enter lastname" ng-model="$root.newAllSurfers.lastname"></td>
							<td><input type="text" class="form-control" placeholder="Enter nickname" ng-model="$root.newAllSurfers.nickname"></td>
							<td>
								<select  ng-model="$root.newAllSurfers.country_id" >
                                    <option ng-repeat="c in countries | object2Array | orderBy:'name'" value="{{ c.id }}">{{ c.name }}</option>
                                </select>
							</td>
							<td><input type="checkbox" class="form-control" ng-model="$root.newAllSurfers.wsl"></td> <!-- ng-true-value="true" ng-false-value="false"-->
							<td><button id="btn-start-wave" class="btn btn-link" type="submit" ng-click="surfer.add($root.newAllSurfers)">Save</button></td>
						</tr>
						<tr ng-repeat="s in surfer.all | object2Array | orderBy:['lastname', 'name']">
							<td>{{ s.id }}</td>
							<td>{{ s.name }}</td>
							<td>{{ s.lastname }}</td>
							<td>{{ s.nickname }}</td>
							<td>{{ $root.countries[s.country_id].name }}</td>
							<td>{{ s.wsl ? 'yes' : 'no' }} <br><button class="btn btn-link" type="submit" ng-click="surfer.update(s.id,{wsl:!s.wsl})">toggle</button></td>
							<td><button class="btn btn-link" type="submit" ng-click="surfer.del(s.id)">Delete</button></td>
						</tr>
					</tbody>
				</table>
            </div>

		</div>


        <!-- Heat sin terminar -->
		<div class="row detailed-info" ng-if="root=='heat' && heat.data.active" >
            <h2>
            Heat <small>#{{ heat.id }}</small>
                <button class="btn btn-warning" type="submit" ng-click="heat.updateStatus(heat.id, 'off')">Finish Heat</button>
                <button class="btn btn-success" type="submit" ng-click="heat.updateStatus(heat.id, 'delay', 5)">Add 5 min</button>
            </h2>
            <div class="row">
                <div class="col-xs-1 col-xs-offset-3">
                    <span unix="{{ heat.data.start_date }}" class="unix-to-until-for-heats"></span>
                </div>
                <div class="col-xs-3">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" style="width: {{ heat.elapsedPer() }}%">
                            <span class="sr-only">20% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1">
                    <span unix="{{ heat.data.finish_date }}" class="unix-to-until-for-heats"></span>
                </div>
            </div>



            <div class="col-xs-3">
                <h3>Surfers
                    <div class="dropdown dropdown-surfers">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        Add
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation" ng-repeat="surfer in event.currentSurfers"><a role="menuitem" tabindex="-1" ng-click="heat.addSurfer(surfer.id)">{{surfer.name}} {{surfer.lastname}}</a></li>
                      </ul>
                    </div>
                </h3>
                <table class="table table-striped table-hover surfers-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fullname</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="part in heat.participations">
                            <td>{{ part.surfer_id }}</td>
                            <td>{{ surfer.get(part.surfer_id).name }} {{ surfer.get(part.surfer_id).lastname }}</td>
                            <td><button class="btn btn-link" type="submit" ng-click="heat.delSurfer(part.surfer_id)">Delete</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
			<div class="col-xs-5">
				<h3>Enabled Waves</h3>
				<table class="table table-striped table-hover rounds-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Finish</th>
							<th>Surfer</th>
							<th># votes</th>
							<th>Score</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody >
						<tr class="info">
							<td>ADD</td>
							<td>In 3 min</td>
							<td>
								<select id="newWaveSurferId" name="fieldName" ng-model="$root.surfer_selected" class="optional overall classes" data-btn-class="option toggle classes btn-info">
									<option value="{{part.surfer_id}}" ng-repeat="part in heat.participations">{{ surfer.get(part.surfer_id).name }}</option>
								</select>
							</td>
                            <td></td>
                            <td></td>
							<td><button id="btn-start-wave" class="btn btn-link" type="submit" ng-click="wave.new($root.surfer_selected)">Start</button></td>
						</tr>
						<tr ng-repeat="w in wave.all | onlyActive | object2Array | orderBy:'finish_date'">
							<td>{{ w.id }}</td>
							<td unix="{{ w.finish_date }}" class="unix-to-until"></td>
							<td>{{ w.surfer.nickname }}</td>
							<td>{{ w.votes_count }}</td>
							<td>{{ w.score }}</td>
							<td>
                                <button class="btn btn-link" type="submit" ng-click="wave.del(w.id)">Delete</button>
                                <button class="btn btn-link" type="submit" ng-click="wave.finish(w.id)">Finish</button>
                            </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-xs-4">
				<h3>Finished Waves</h3>
				<table class="table table-striped table-hover rounds-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Start</th>
							<th>Surfer</th>
							<th>Number</th>
							<th># votes</th>
							<th>Score</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="w in wave.all | onlyNotActive | object2Array | orderBy:'start_date':true">
							<td>{{ w.id }}</td>
							<td unix="{{ w.start_date }}" class="unix-to-until"></td>
							<td>{{ w.surfer.name }} {{ w.surfer.lastname }}</td>
							<td>{{ w.number }}</td>
							<td>{{ w.votes_count }}</td>
							<td>{{ w.score }}</td>
							<td><button class="btn btn-link" type="submit" ng-click="wave.del(w.id)">Delete</button></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>



        <!-- Heat terminado -->
        <div class="row detailed-info" ng-if="root=='heat' && !heat.data.active">
            <h2>Heat Results <small>#{{ heat.id }}</small>
                <button type="button" class="btn" ng-class="{'btn-info': !forceUpdateScores_status, 'btn-warning': forceUpdateScores_status == 'sending', 'btn-success': forceUpdateScores_status == 'ok'}" ng-click="heat.forceUpdateResults()">{{ !forceUpdateScores_status ? 'Update Scores' : (forceUpdateScores_status == 'ok' ? 'Updated!' : 'Sending') }}</button>
                <button class="btn btn-success" type="submit" ng-click="heat.updateStatus(heat.id, 'delay', 5)">Reopen 5 mins more</button>
            </h2>
            <div class="col-xs-6">
                <h3>Surfers</h3>
                <table class="table table-striped table-hover surfers-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>fullname</th>
                            <th>score_wst</th>
                            <th>score_asp</th>
                            <th>Update Score ASP</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="part in heat.participations | object2Array | orderBy:'part.score_wst':true">
                            <td>{{ part.surfer_id }}</td>
                            <td>{{ surfer.get(part.surfer_id).name }} {{ surfer.get(part.surfer_id).lastname }}</td>
                            <td>{{ part.score_wst }}</td>
                            <td>{{ part.score_asp }}</td>
                            <td>

                                <div style="width:150px;">
                                    <div class="input-group">
                                      <input type="text" class="form-control" placeholder="" ng-model="formNewOfficialScore" ng-keyup="$event.keyCode == 13 && heat.setOfficialScore(part.surfer_id, formNewOfficialScore)">
                                      <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" ng-click="heat.setOfficialScore(part.surfer_id, formNewOfficialScore)">Send!</button>
                                      </span>
                                    </div><!-- /input-group -->
                                </div>


                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-6">
                <h3>Waves</h3>
                <table class="table table-striped table-hover rounds-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Start</th>
                            <th>Surfer</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="wave in wave.all | object2Array | orderBy:'start_date':true">
                            <td>{{ wave.id }}</td>
                            <td unix="{{ wave.start_date }}" class="unix-to-until"></td>
                            <td>{{ wave.surfer.nickname }}</td>
                            <td>{{ wave.score }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>



      <footer>
        <p>&copy; World Surf Team</p>
      </footer>

    </div> <!-- /container -->


    <div class="modal fade" id="modal_errors" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">New message</h4>
		  </div>
		  <div class="modal-body">

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>


	<div class="modal fade" id="modal_create_heat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" ng-init="$root.newSurfers = []">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Create Heat</h4>
		  </div>
		  <div class="modal-body">

			  <h3>Select Surfers</h3>
			  <div class="dropdown dropdown-surfers" >
				  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
					Select
					<span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
					<li role="presentation" ng-repeat="surfer in event.currentSurfers | notIn:$root.newSurfers | object2Array | orderBy:['lastname', 'name']"><a role="menuitem" tabindex="-1" ng-click="$root.newSurfers.indexOf(surfer.id) == -1 ? $root.newSurfers.push(surfer.id) : false">{{surfer.name}} {{surfer.lastname}}</a></li>
				  </ul>
			</div>

			  <table class="table table-striped table-hover surfers-table">
				<thead>
					<tr>
						<th>ID</th>
						<th>fullname</th>
						<th>country</th>
						<th>Selection</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="surfer_id in $root.newSurfers">
						<td>{{ surfer_id }}</td>
						<td>{{ surfer.all[surfer_id].name }} {{ surfer.all[surfer_id].lastname }}</td>
						<td>{{ $root.countries[surfer.all[surfer_id].country_id].name }}</td>
						<td><button class="btn btn-link" type="submit" ng-click="$root.newSurfers.splice($root.newSurfers.indexOf(surfer_id),1)">Delete</button></td>
					</tr>
				</tbody>
			</table>


		  </div>
		  <div class="modal-footer">
			  <button type="button" class="btn btn-primary" ng-click="heat.new($root.newSurfers)">Send</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>



    <!-- Environment -->
    <script charset="utf-8">
      var server_env = '<% $env %>';
    </script>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/routie.min.js"></script>
        <script src="js/vendor/countdown.min.js"></script>
        <script src="js/vendor/md5.js"></script>
        <script src="js/vendor/utf8_encode.js"></script>
        <script src="js/vendor/moment-with-locales.min.js"></script>
        <script src="js/vendor/bootstrap-datepicker.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jStorage/0.4.12/jstorage.min.js"></script>
        <script src="http://rawgithub.com/cchantep/bootstrap-combobox/master/js/bootstrap-combobox.min.js"></script>
		<!--
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.6.0/ui-bootstrap-tpls.min.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/datePicker.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/datePickerUtils.js"></script>
		<script src="https://rawgit.com/g00fy-/angular-datepicker/master/app/scripts/dateRange.js"></script>

-->
         <script src="js/general.js"></script>


        <script src="static-admin/js/ready.js"></script>
		<script src="static-admin/js/app.js"></script>
		<script src="static-admin/js/routes.js"></script>
		<script src="static-admin/js/filters.js"></script>

        <!-- Models -->
		<script src="static-admin/js/models/user.js"></script>
		<script src="static-admin/js/models/event.js"></script>
		<script src="static-admin/js/models/surfer.js"></script>
		<script src="static-admin/js/models/heat.js"></script>
		<script src="static-admin/js/models/wave.js"></script>

		<!-- controllers -->
		<script src="static-admin/js/mainCtrl.js"></script>





    </body>
 </html>
