@extends('master')


@section('content')

    @foreach($songs as $i => $song)
        <li><a href="/songs/{{ $song->slug }}">{{ $song->title }}</a></li>
    @endforeach

@stop
