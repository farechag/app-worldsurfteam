@extends('master')


@section('content')

    <h2>{{ $song->title }}</h2>
    <br>

    @if($song->lyrics)
        <article>{!! nl2br($song->lyrics) !!}</article>
    @endif
@stop
