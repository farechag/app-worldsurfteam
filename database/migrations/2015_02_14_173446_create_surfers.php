<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurfers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('surfers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->string('lastname', 50);
			$table->string('nickname', 50);
			$table->string('description', 140)->nullable();

			$table->integer('country_id')->nullable()->unsigned();
			$table->foreign('country_id')->references('id')->on('countries')
				->onDelete('set null');

			$table->boolean('wsl', 15)->default(false);

			$table->char('pic_code', 15)->nullable();
			$table->string('pic_mime', 200)->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('surfers');
	}

}
