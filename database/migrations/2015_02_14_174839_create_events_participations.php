	<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsParticipations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events_participations', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')
				->onDelete('cascade');

			$table->integer('surfer_id')->unsigned();
			$table->foreign('surfer_id')->references('id')->on('surfers')
				->onDelete('cascade');

			$table->unique(['event_id', 'surfer_id']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('events_participations');
	}

}
