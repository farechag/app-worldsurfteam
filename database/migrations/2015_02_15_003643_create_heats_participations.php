<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeatsParticipations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('heats_participations', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('heat_id')->unsigned();
			$table->foreign('heat_id')->references('id')->on('heats')
				->onDelete('cascade');

			$table->integer('surfer_id')->unsigned();
			$table->foreign('surfer_id')->references('id')->on('surfers')
				->onDelete('cascade');

			$table->float('score_wst')->nullable();
			$table->float('score_asp')->nullable();

			$table->unique(['heat_id', 'surfer_id']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('heats_participations');
	}

}
