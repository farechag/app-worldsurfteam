<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAddSkills extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->integer('skill_id')->unsigned()->nullable()->default(null);
			$table->foreign('skill_id')->references('id')->on('users_skills')
				->onDelete('set null');
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::table('users', function($table)
		{
			$table->dropForeign('users_skill_id_foreign');
			$table->dropColumn('skill_id');
		});
	}

}
