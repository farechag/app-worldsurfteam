<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('votes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');

			$table->integer('wave_id')->unsigned();
			$table->foreign('wave_id')->references('id')->on('waves')
				->onDelete('cascade');

			$table->float('points');

			$table->integer('date')->unsigned()->nullable();

			$table->unique(['user_id', 'wave_id']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('votes');
	}

}
