<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeats extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('heats', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('round_id')->unsigned();
			$table->foreign('round_id')->references('id')->on('rounds')
				->onDelete('cascade');

			$table->integer('start_date')->unsigned()->nullable();
			$table->integer('finish_date')->unsigned()->nullable();

			$table->integer('number')->unsigned();

			$table->boolean('finished')->default(false);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('heats');
	}

}
