<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvents extends Migration
{

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('start_date')->unsigned()->nullable();
			$table->integer('finish_date')->unsigned()->nullable();
			$table->string('start_date_human')->nullable();
			$table->string('finish_date_human')->nullable();
			$table->string('location')->nullable();

			$table->integer('type_id')->nullable()->unsigned();
			$table->foreign('type_id')->references('id')->on('events_types')
				->onDelete('set null');

			$table->integer('sponsor_id')->nullable()->unsigned();
			$table->foreign('sponsor_id')->references('id')->on('events_sponsors')
				->onDelete('set null');

			$table->integer('country_id')->nullable()->unsigned();
			$table->foreign('country_id')->references('id')->on('countries')
				->onDelete('set null');

			$table->integer('timezone_offset')->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('events');
	}

}
