<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaves extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('waves', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('start_date')->unsigned()->nullable();
			$table->integer('finish_date')->unsigned()->nullable();

			$table->integer('heat_id')->unsigned();
			$table->foreign('heat_id')->references('id')->on('heats')
				->onDelete('cascade');

			$table->integer('surfer_id')->unsigned();
			$table->foreign('surfer_id')->references('id')->on('surfers')
				->onDelete('cascade');



			$table->integer('number')->unsigned();
			$table->float('score')->nullable();
			$table->integer('votes_count')->unsigned();

			$table->unique(['heat_id', 'surfer_id', 'start_date']);
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('waves');
	}

}
