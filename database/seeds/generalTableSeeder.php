<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Event;
use App\EventType;
use App\EventSponsor;
use App\Surfer;
use App\EventParticipation;
use App\Round;
use App\Heat;
use App\HeatParticipation;
use App\UserSkill;
use App\Wave;
use App\Vote;

use Riogo\Permiso\Role;
use Riogo\Permiso\Permission;

use Illuminate\Contracts\Auth\Registrar;

class generalTableSeeder extends Seeder
{

    protected $registrar;

    public function __construct(Registrar $registrar)
    {
        $this->registrar = $registrar;
    }



    public function run()
    {

        $this->registrar->create([  'name' => 'juan',
                                    'email'=> 'juan@juan.com',
                                    'password'=> 'pass1']);
        $this->registrar->create([  'name' => 'ana',
                                    'email'=> 'ana@ana.com',
                                    'password'=> 'secret']);

        UserSkill::create(['name' => 'noob']);
        UserSkill::create(['name' => 'medium']);
        UserSkill::create(['name' => 'pro']);
        UserSkill::create(['name' => 'godlike']);

        EventType::create([ 'name' => 'Tipo 1']);
        EventSponsor::create([ 'name' => 'Redbull']);
        Event::create([ 'name'  => 'El super evento de Surf.',
                        'location'=> 'valencia',
                        'type_id' => 1,
                        'sponsor_id' => 1,
                        'country_id' => 1]);
        Event::create([ 'name'  => 'Un evento de surf menos molón',
                        'location'=> 'portugal',
                        'type_id' => 1,
                        'sponsor_id' => 1,
                        'country_id' => 1]);
        Surfer::create([    'name'  => 'Juan Antonio',
                            'country_id'    => 1 ]);
        Surfer::create([    'name'  => 'María Gonzalez',
                            'country_id'    => 2 ]);

        EventParticipation::create([ 'surfer_id' => 1,
                                    'event_id' => 1]);
        EventParticipation::create([ 'surfer_id' => 2,
                                    'event_id' => 1]);
        EventParticipation::create([ 'surfer_id' => 1,
                                    'event_id' => 2]);

        Round::create(['event_id' => 1, 'number' => 1]);
        Round::create(['event_id' => 1, 'number' => 2]);
        Round::create(['event_id' => 1, 'number' => 3]);
        Round::create(['event_id' => 2, 'number' => 1]);
        Round::create(['event_id' => 2, 'number' => 2]);

        Heat::create([  'round_id' => 1 ]);
        Heat::create([  'round_id' => 2 ]);
        Heat::create([  'round_id' => 4 ]);

        HeatParticipation::create([     'heat_id' => 1,
                                        'surfer_id' => 1]);
        HeatParticipation::create([     'heat_id' => 1,
                                        'surfer_id' => 2]);
        HeatParticipation::create([     'heat_id' => 3,
                                        'surfer_id' => 1]);


        Wave::create([  'heat_id'   => 1,
                        'surfer_id' => 1]);
        Wave::create([  'heat_id'   => 1,
                        'surfer_id' => 2]);


        Vote::create(   [ 'wave_id' => 1,
                            'user_id' => 1,
                            'points'    => 3]);
        Vote::create(   [ 'wave_id' => 2,
                            'user_id' => 1,
                            'points'    => 9]);
        Vote::create(   [ 'wave_id' => 2,
                            'user_id' => 2,
                            'points'    => 4]);


        Role::create([  'name'          => 'user',
                        'display_name'  => 'Usuario',
                        'description'   => 'Usuario normal.']);

        Role::create([  'name'          => 'admin',
                        'display_name'  => 'Administrador',
                        'description'   => 'Admin con poderes.']);


    }

}
