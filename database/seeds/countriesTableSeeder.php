<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;

    class countriesTableSeeder extends Seeder {

        public function run()
        {
            // Recommended when importing larger CSVs
            DB::disableQueryLog();

            // Uncomment the below to wipe the table clean before populating
            //DB::table($this->table)->truncate();

            DB::statement(file_get_contents(base_path().'/database/seeds/sqls/countries.sql'));

        }
    }


?>
