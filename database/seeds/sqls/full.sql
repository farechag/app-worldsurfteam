DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `continent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `countries` (`id`, `code`, `name`, `capital`, `continent`, `code3`)
VALUES
	(1,'AD','Andorra','Andorra la Vella','EU','AND'),
	(2,'AE','United Arab Emirates','Abu Dhabi','AS','ARE'),
	(3,'AF','Afghanistan','Kabul','AS','AFG'),
	(4,'AG','Antigua and Barbuda','St. John\'s','NA','ATG'),
	(5,'AI','Anguilla','The Valley','NA','AIA'),
	(6,'AL','Albania','Tirana','EU','ALB'),
	(7,'AM','Armenia','Yerevan','AS','ARM'),
	(8,'AN','','Willemstad','NA','ANT'),
	(9,'AO','Angola','Luanda','AF','AGO'),
	(10,'AQ','Antarctica','','AN','ATA'),
	(11,'AR','Argentina','Buenos Aires','SA','ARG'),
	(12,'AS','American Samoa','Pago Pago','OC','ASM'),
	(13,'AT','Austria','Vienna','EU','AUT'),
	(14,'AU','Australia','Canberra','OC','AUS'),
	(15,'AW','Aruba','Oranjestad','NA','ABW'),
	(16,'AX','Åland','Mariehamn','EU','ALA'),
	(17,'AZ','Azerbaijan','Baku','AS','AZE'),
	(18,'BA','Bosnia and Herzegovina','Sarajevo','EU','BIH'),
	(19,'BB','Barbados','Bridgetown','NA','BRB'),
	(20,'BD','Bangladesh','Dhaka','AS','BGD'),
	(21,'BE','Belgium','Brussels','EU','BEL'),
	(22,'BF','Burkina Faso','Ouagadougou','AF','BFA'),
	(23,'BG','Bulgaria','Sofia','EU','BGR'),
	(24,'BH','Bahrain','Manama','AS','BHR'),
	(25,'BI','Burundi','Bujumbura','AF','BDI'),
	(26,'BJ','Benin','Porto-Novo','AF','BEN'),
	(27,'BL','Saint Barthélemy','Gustavia','NA','BLM'),
	(28,'BM','Bermuda','Hamilton','NA','BMU'),
	(29,'BN','Brunei','Bandar Seri Begawan','AS','BRN'),
	(30,'BO','Bolivia','Sucre','SA','BOL'),
	(31,'BQ','Bonaire','','NA','BES'),
	(32,'BR','Brazil','Brasilia','SA','BRA'),
	(33,'BS','Bahamas','Nassau','NA','BHS'),
	(34,'BT','Bhutan','Thimphu','AS','BTN'),
	(35,'BV','Bouvet Island','','AN','BVT'),
	(36,'BW','Botswana','Gaborone','AF','BWA'),
	(37,'BY','Belarus','Minsk','EU','BLR'),
	(38,'BZ','Belize','Belmopan','NA','BLZ'),
	(39,'CA','Canada','Ottawa','NA','CAN'),
	(40,'CC','Cocos [Keeling] Islands','West Island','AS','CCK'),
	(41,'CD','Democratic Republic of the Congo','Kinshasa','AF','COD'),
	(42,'CF','Central African Republic','Bangui','AF','CAF'),
	(43,'CG','Republic of the Congo','Brazzaville','AF','COG'),
	(44,'CH','Switzerland','Berne','EU','CHE'),
	(45,'CI','Ivory Coast','Yamoussoukro','AF','CIV'),
	(46,'CK','Cook Islands','Avarua','OC','COK'),
	(47,'CL','Chile','Santiago','SA','CHL'),
	(48,'CM','Cameroon','Yaounde','AF','CMR'),
	(49,'CN','China','Beijing','AS','CHN'),
	(50,'CO','Colombia','Bogota','SA','COL'),
	(51,'CR','Costa Rica','San Jose','NA','CRI'),
	(52,'CS','','Belgrade','EU','SCG'),
	(53,'CU','Cuba','Havana','NA','CUB'),
	(54,'CV','Cape Verde','Praia','AF','CPV'),
	(55,'CW','Curacao','Willemstad','NA','CUW'),
	(56,'CX','Christmas Island','Flying Fish Cove','AS','CXR'),
	(57,'CY','Cyprus','Nicosia','EU','CYP'),
	(58,'CZ','Czech Republic','Prague','EU','CZE'),
	(59,'DE','Germany','Berlin','EU','DEU'),
	(60,'DJ','Djibouti','Djibouti','AF','DJI'),
	(61,'DK','Denmark','Copenhagen','EU','DNK'),
	(62,'DM','Dominica','Roseau','NA','DMA'),
	(63,'DO','Dominican Republic','Santo Domingo','NA','DOM'),
	(64,'DZ','Algeria','Algiers','AF','DZA'),
	(65,'EC','Ecuador','Quito','SA','ECU'),
	(66,'EE','Estonia','Tallinn','EU','EST'),
	(67,'EG','Egypt','Cairo','AF','EGY'),
	(68,'EH','Western Sahara','El-Aaiun','AF','ESH'),
	(69,'ER','Eritrea','Asmara','AF','ERI'),
	(70,'ES','Spain','Madrid','EU','ESP'),
	(71,'ET','Ethiopia','Addis Ababa','AF','ETH'),
	(72,'FI','Finland','Helsinki','EU','FIN'),
	(73,'FJ','Fiji','Suva','OC','FJI'),
	(74,'FK','Falkland Islands','Stanley','SA','FLK'),
	(75,'FM','Micronesia','Palikir','OC','FSM'),
	(76,'FO','Faroe Islands','Torshavn','EU','FRO'),
	(77,'FR','France','Paris','EU','FRA'),
	(78,'GA','Gabon','Libreville','AF','GAB'),
	(79,'GB','United Kingdom','London','EU','GBR'),
	(80,'GD','Grenada','St. George\'s','NA','GRD'),
	(81,'GE','Georgia','Tbilisi','AS','GEO'),
	(82,'GF','French Guiana','Cayenne','SA','GUF'),
	(83,'GG','Guernsey','St Peter Port','EU','GGY'),
	(84,'GH','Ghana','Accra','AF','GHA'),
	(85,'GI','Gibraltar','Gibraltar','EU','GIB'),
	(86,'GL','Greenland','Nuuk','NA','GRL'),
	(87,'GM','Gambia','Banjul','AF','GMB'),
	(88,'GN','Guinea','Conakry','AF','GIN'),
	(89,'GP','Guadeloupe','Basse-Terre','NA','GLP'),
	(90,'GQ','Equatorial Guinea','Malabo','AF','GNQ'),
	(91,'GR','Greece','Athens','EU','GRC'),
	(92,'GS','South Georgia and the South Sandwich Islands','Grytviken','AN','SGS'),
	(93,'GT','Guatemala','Guatemala City','NA','GTM'),
	(94,'GU','Guam','Hagatna','OC','GUM'),
	(95,'GW','Guinea-Bissau','Bissau','AF','GNB'),
	(96,'GY','Guyana','Georgetown','SA','GUY'),
	(97,'HK','Hong Kong','Hong Kong','AS','HKG'),
	(98,'HM','Heard Island and McDonald Islands','','AN','HMD'),
	(99,'HN','Honduras','Tegucigalpa','NA','HND'),
	(100,'HR','Croatia','Zagreb','EU','HRV'),
	(101,'HT','Haiti','Port-au-Prince','NA','HTI'),
	(102,'HU','Hungary','Budapest','EU','HUN'),
	(103,'ID','Indonesia','Jakarta','AS','IDN'),
	(104,'IE','Ireland','Dublin','EU','IRL'),
	(105,'IL','Israel','Jerusalem','AS','ISR'),
	(106,'IM','Isle of Man','Douglas, Isle of Man','EU','IMN'),
	(107,'IN','India','New Delhi','AS','IND'),
	(108,'IO','British Indian Ocean Territory','Diego Garcia','AS','IOT'),
	(109,'IQ','Iraq','Baghdad','AS','IRQ'),
	(110,'IR','Iran','Tehran','AS','IRN'),
	(111,'IS','Iceland','Reykjavik','EU','ISL'),
	(112,'IT','Italy','Rome','EU','ITA'),
	(113,'JE','Jersey','Saint Helier','EU','JEY'),
	(114,'JM','Jamaica','Kingston','NA','JAM'),
	(115,'JO','Jordan','Amman','AS','JOR'),
	(116,'JP','Japan','Tokyo','AS','JPN'),
	(117,'KE','Kenya','Nairobi','AF','KEN'),
	(118,'KG','Kyrgyzstan','Bishkek','AS','KGZ'),
	(119,'KH','Cambodia','Phnom Penh','AS','KHM'),
	(120,'KI','Kiribati','Tarawa','OC','KIR'),
	(121,'KM','Comoros','Moroni','AF','COM'),
	(122,'KN','Saint Kitts and Nevis','Basseterre','NA','KNA'),
	(123,'KP','North Korea','Pyongyang','AS','PRK'),
	(124,'KR','South Korea','Seoul','AS','KOR'),
	(125,'KW','Kuwait','Kuwait City','AS','KWT'),
	(126,'KY','Cayman Islands','George Town','NA','CYM'),
	(127,'KZ','Kazakhstan','Astana','AS','KAZ'),
	(128,'LA','Laos','Vientiane','AS','LAO'),
	(129,'LB','Lebanon','Beirut','AS','LBN'),
	(130,'LC','Saint Lucia','Castries','NA','LCA'),
	(131,'LI','Liechtenstein','Vaduz','EU','LIE'),
	(132,'LK','Sri Lanka','Colombo','AS','LKA'),
	(133,'LR','Liberia','Monrovia','AF','LBR'),
	(134,'LS','Lesotho','Maseru','AF','LSO'),
	(135,'LT','Lithuania','Vilnius','EU','LTU'),
	(136,'LU','Luxembourg','Luxembourg','EU','LUX'),
	(137,'LV','Latvia','Riga','EU','LVA'),
	(138,'LY','Libya','Tripolis','AF','LBY'),
	(139,'MA','Morocco','Rabat','AF','MAR'),
	(140,'MC','Monaco','Monaco','EU','MCO'),
	(141,'MD','Moldova','Chisinau','EU','MDA'),
	(142,'ME','Montenegro','Podgorica','EU','MNE'),
	(143,'MF','Saint Martin','Marigot','NA','MAF'),
	(144,'MG','Madagascar','Antananarivo','AF','MDG'),
	(145,'MH','Marshall Islands','Majuro','OC','MHL'),
	(146,'MK','Macedonia','Skopje','EU','MKD'),
	(147,'ML','Mali','Bamako','AF','MLI'),
	(148,'MM','Myanmar [Burma]','Nay Pyi Taw','AS','MMR'),
	(149,'MN','Mongolia','Ulan Bator','AS','MNG'),
	(150,'MO','Macao','Macao','AS','MAC'),
	(151,'MP','Northern Mariana Islands','Saipan','OC','MNP'),
	(152,'MQ','Martinique','Fort-de-France','NA','MTQ'),
	(153,'MR','Mauritania','Nouakchott','AF','MRT'),
	(154,'MS','Montserrat','Plymouth','NA','MSR'),
	(155,'MT','Malta','Valletta','EU','MLT'),
	(156,'MU','Mauritius','Port Louis','AF','MUS'),
	(157,'MV','Maldives','Male','AS','MDV'),
	(158,'MW','Malawi','Lilongwe','AF','MWI'),
	(159,'MX','Mexico','Mexico City','NA','MEX'),
	(160,'MY','Malaysia','Kuala Lumpur','AS','MYS'),
	(161,'MZ','Mozambique','Maputo','AF','MOZ'),
	(162,'NA','Namibia','Windhoek','AF','NAM'),
	(163,'NC','New Caledonia','Noumea','OC','NCL'),
	(164,'NE','Niger','Niamey','AF','NER'),
	(165,'NF','Norfolk Island','Kingston','OC','NFK'),
	(166,'NG','Nigeria','Abuja','AF','NGA'),
	(167,'NI','Nicaragua','Managua','NA','NIC'),
	(168,'NL','Netherlands','Amsterdam','EU','NLD'),
	(169,'NO','Norway','Oslo','EU','NOR'),
	(170,'NP','Nepal','Kathmandu','AS','NPL'),
	(171,'NR','Nauru','Yaren','OC','NRU'),
	(172,'NU','Niue','Alofi','OC','NIU'),
	(173,'NZ','New Zealand','Wellington','OC','NZL'),
	(174,'OM','Oman','Muscat','AS','OMN'),
	(175,'PA','Panama','Panama City','NA','PAN'),
	(176,'PE','Peru','Lima','SA','PER'),
	(177,'PF','French Polynesia','Papeete','OC','PYF'),
	(178,'PG','Papua New Guinea','Port Moresby','OC','PNG'),
	(179,'PH','Philippines','Manila','AS','PHL'),
	(180,'PK','Pakistan','Islamabad','AS','PAK'),
	(181,'PL','Poland','Warsaw','EU','POL'),
	(182,'PM','Saint Pierre and Miquelon','Saint-Pierre','NA','SPM'),
	(183,'PN','Pitcairn Islands','Adamstown','OC','PCN'),
	(184,'PR','Puerto Rico','San Juan','NA','PRI'),
	(185,'PS','Palestine','East Jerusalem','AS','PSE'),
	(186,'PT','Portugal','Lisbon','EU','PRT'),
	(187,'PW','Palau','Melekeok','OC','PLW'),
	(188,'PY','Paraguay','Asuncion','SA','PRY'),
	(189,'QA','Qatar','Doha','AS','QAT'),
	(190,'RE','Réunion','Saint-Denis','AF','REU'),
	(191,'RO','Romania','Bucharest','EU','ROU'),
	(192,'RS','Serbia','Belgrade','EU','SRB'),
	(193,'RU','Russia','Moscow','EU','RUS'),
	(194,'RW','Rwanda','Kigali','AF','RWA'),
	(195,'SA','Saudi Arabia','Riyadh','AS','SAU'),
	(196,'SB','Solomon Islands','Honiara','OC','SLB'),
	(197,'SC','Seychelles','Victoria','AF','SYC'),
	(198,'SD','Sudan','Khartoum','AF','SDN'),
	(199,'SE','Sweden','Stockholm','EU','SWE'),
	(200,'SG','Singapore','Singapur','AS','SGP'),
	(201,'SH','Saint Helena','Jamestown','AF','SHN'),
	(202,'SI','Slovenia','Ljubljana','EU','SVN'),
	(203,'SJ','Svalbard and Jan Mayen','Longyearbyen','EU','SJM'),
	(204,'SK','Slovakia','Bratislava','EU','SVK'),
	(205,'SL','Sierra Leone','Freetown','AF','SLE'),
	(206,'SM','San Marino','San Marino','EU','SMR'),
	(207,'SN','Senegal','Dakar','AF','SEN'),
	(208,'SO','Somalia','Mogadishu','AF','SOM'),
	(209,'SR','Suriname','Paramaribo','SA','SUR'),
	(210,'SS','South Sudan','Juba','AF','SSD'),
	(211,'ST','São Tomé and Príncipe','Sao Tome','AF','STP'),
	(212,'SV','El Salvador','San Salvador','NA','SLV'),
	(213,'SX','Sint Maarten','Philipsburg','NA','SXM'),
	(214,'SY','Syria','Damascus','AS','SYR'),
	(215,'SZ','Swaziland','Mbabane','AF','SWZ'),
	(216,'TC','Turks and Caicos Islands','Cockburn Town','NA','TCA'),
	(217,'TD','Chad','N\'Djamena','AF','TCD'),
	(218,'TF','French Southern Territories','Port-aux-Francais','AN','ATF'),
	(219,'TG','Togo','Lome','AF','TGO'),
	(220,'TH','Thailand','Bangkok','AS','THA'),
	(221,'TJ','Tajikistan','Dushanbe','AS','TJK'),
	(222,'TK','Tokelau','','OC','TKL'),
	(223,'TL','East Timor','Dili','OC','TLS'),
	(224,'TM','Turkmenistan','Ashgabat','AS','TKM'),
	(225,'TN','Tunisia','Tunis','AF','TUN'),
	(226,'TO','Tonga','Nuku\'alofa','OC','TON'),
	(227,'TR','Turkey','Ankara','AS','TUR'),
	(228,'TT','Trinidad and Tobago','Port of Spain','NA','TTO'),
	(229,'TV','Tuvalu','Funafuti','OC','TUV'),
	(230,'TW','Taiwan','Taipei','AS','TWN'),
	(231,'TZ','Tanzania','Dodoma','AF','TZA'),
	(232,'UA','Ukraine','Kiev','EU','UKR'),
	(233,'UG','Uganda','Kampala','AF','UGA'),
	(234,'UM','U.S. Minor Outlying Islands','','OC','UMI'),
	(235,'US','United States','Washington','NA','USA'),
	(236,'UY','Uruguay','Montevideo','SA','URY'),
	(237,'UZ','Uzbekistan','Tashkent','AS','UZB'),
	(238,'VA','Vatican City','Vatican City','EU','VAT'),
	(239,'VC','Saint Vincent and the Grenadines','Kingstown','NA','VCT'),
	(240,'VE','Venezuela','Caracas','SA','VEN'),
	(241,'VG','British Virgin Islands','Road Town','NA','VGB'),
	(242,'VI','U.S. Virgin Islands','Charlotte Amalie','NA','VIR'),
	(243,'VN','Vietnam','Hanoi','AS','VNM'),
	(244,'VU','Vanuatu','Port Vila','OC','VUT'),
	(245,'WF','Wallis and Futuna','Mata Utu','OC','WLF'),
	(246,'WS','Samoa','Apia','OC','WSM'),
	(247,'XK','Kosovo','Pristina','EU','XKX'),
	(248,'YE','Yemen','Sanaa','AS','YEM'),
	(249,'YT','Mayotte','Mamoudzou','AF','MYT'),
	(250,'ZA','South Africa','Pretoria','AF','ZAF'),
	(251,'ZM','Zambia','Lusaka','AF','ZMB'),
	(252,'ZW','Zimbabwe','Harare','AF','ZWE');



# Volcado de tabla events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(10) unsigned DEFAULT NULL,
  `finish_date` int(10) unsigned DEFAULT NULL,
  `start_date_human` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finish_date_human` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `sponsor_id` int(10) unsigned DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_type_id_foreign` (`type_id`),
  KEY `events_sponsor_id_foreign` (`sponsor_id`),
  KEY `events_country_id_foreign` (`country_id`),
  CONSTRAINT `events_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `events_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `events_sponsors` (`id`) ON DELETE SET NULL,
  CONSTRAINT `events_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `events_types` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `events` (`id`, `name`, `start_date`, `finish_date`, `start_date_human`, `finish_date_human`, `location`, `type_id`, `sponsor_id`, `country_id`)
VALUES
	(1,'sdfsdf',NULL,1434778523,NULL,NULL,NULL,NULL,NULL,1),
	(3,'Evento molón de surf',1424785732,1424785732,'2015-01-15 15:52:01','2015-03-15 15:52:01','1',NULL,NULL,1),
	(4,'Evento molón de surf',1424785762,1424785762,'2015-01-15 15:52:01','2015-03-15 15:52:01','1',NULL,NULL,1),
	(5,'Evento molón de surf',1424785819,1424785819,'2015-02-24 14:50:19','2015-02-24 15:10:19','1',NULL,NULL,1),
	(6,'Evento molón de surf',1424786304,1424787504,'2015-02-24 14:58:24','2015-02-24 15:18:24','1',NULL,NULL,1),
	(7,'Evento molón de surf',1424786335,1424787535,'2015-02-24T14:58:55+01:00','2015-02-24T15:18:55+01:00','1',NULL,NULL,1),
	(8,'Evento molón de surf',1424786464,1424787664,'24-02-2015','24-02-2015','1',NULL,NULL,1),
	(9,'Evento molón de surf',1425942000,1424787775,'10-03-2015','24-02-2015','1',NULL,NULL,1),
	(10,'Evento molón de surf',1,1,'01-01-1970','01-01-1970','1',NULL,NULL,1),
	(11,'Evento molón de surf',1424786954,1434786954,'24-02-2015','20-06-2015','Allí lejos',NULL,NULL,1);




# Volcado de tabla events_participations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events_participations`;

CREATE TABLE `events_participations` (
  `event_id` int(10) unsigned NOT NULL,
  `surfer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`event_id`,`surfer_id`),
  KEY `events_participations_surfer_id_foreign` (`surfer_id`),
  CONSTRAINT `events_participations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  CONSTRAINT `events_participations_surfer_id_foreign` FOREIGN KEY (`surfer_id`) REFERENCES `surfers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `events_participations` (`event_id`, `surfer_id`)
VALUES
	(1,1),
	(3,1),
	(3,2);



# Volcado de tabla events_sponsors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events_sponsors`;

CREATE TABLE `events_sponsors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla events_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events_types`;

CREATE TABLE `events_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla heats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `heats`;

CREATE TABLE `heats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `round_id` int(10) unsigned NOT NULL,
  `start_date` int(11) unsigned DEFAULT NULL,
  `finish_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `heats_round_id_foreign` (`round_id`),
  CONSTRAINT `heats_round_id_foreign` FOREIGN KEY (`round_id`) REFERENCES `rounds` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `heats` (`id`, `round_id`, `start_date`, `finish_date`)
VALUES
	(1,3,0,NULL);



# Volcado de tabla heats_participations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `heats_participations`;

CREATE TABLE `heats_participations` (
  `heat_id` int(10) unsigned NOT NULL,
  `surfer_id` int(10) unsigned NOT NULL,
  `score_wst` double(8,2) DEFAULT NULL,
  `score_asp` double(8,2) DEFAULT NULL,
  PRIMARY KEY (`heat_id`,`surfer_id`),
  KEY `heats_participations_surfer_id_foreign` (`surfer_id`),
  CONSTRAINT `heats_participations_heat_id_foreign` FOREIGN KEY (`heat_id`) REFERENCES `heats` (`id`) ON DELETE CASCADE,
  CONSTRAINT `heats_participations_surfer_id_foreign` FOREIGN KEY (`surfer_id`) REFERENCES `surfers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `heats_participations` (`heat_id`, `surfer_id`, `score_wst`, `score_asp`)
VALUES
	(1,1,NULL,5.50),
	(1,2,NULL,5.50);






# Volcado de tabla permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla role_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `role_permission_permission_id_foreign` (`permission_id`),
  CONSTRAINT `role_permission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla rounds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rounds`;

CREATE TABLE `rounds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rounds_event_id_foreign` (`event_id`),
  CONSTRAINT `rounds_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `rounds` (`id`, `event_id`, `number`)
VALUES
	(1,1,'1'),
	(2,1,'2'),
	(3,1,'3'),
	(4,1,'Quarters'),
	(5,1,'Semis'),
	(6,1,'Final');




# Volcado de tabla surfers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surfers`;

CREATE TABLE `surfers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(140) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `pic_code` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_mime` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `surfers_country_id_foreign` (`country_id`),
  CONSTRAINT `surfers_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `surfers` (`id`, `name`, `lastname`, `description`, `country_id`, `pic_code`, `pic_mime`)
VALUES
	(1,'asd','',NULL,NULL,NULL,NULL),
	(2,'dfs','',NULL,NULL,NULL,NULL);




# Volcado de tabla user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_role_role_id_foreign` (`role_id`),
  CONSTRAINT `user_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `user_role_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(10) unsigned DEFAULT NULL,
  `skill_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_country_id_foreign` (`country_id`),
  KEY `users_skill_id_foreign` (`skill_id`),
  CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `users_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `users_skills` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `country_id`, `skill_id`)
VALUES
	(2,'Juan García','ana@ana.com','$2y$10$GY8KZHxOsQkjHPQGh95hR.1wGlDVorXPt8GO8OrX4BoBeKpOkJqUy',NULL,'2015-02-24 12:55:43','2015-02-24 12:55:43',1,NULL);




# Volcado de tabla users_skills
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_skills`;

CREATE TABLE `users_skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `votes`;

CREATE TABLE `votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `wave_id` int(10) unsigned NOT NULL,
  `points` double(8,2) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `votes_user_id_wave_id_unique` (`user_id`,`wave_id`),
  KEY `votes_wave_id_foreign` (`wave_id`),
  CONSTRAINT `votes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `votes_wave_id_foreign` FOREIGN KEY (`wave_id`) REFERENCES `waves` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Volcado de tabla waves
# ------------------------------------------------------------

DROP TABLE IF EXISTS `waves`;

CREATE TABLE `waves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `heat_id` int(10) unsigned NOT NULL,
  `surfer_id` int(10) unsigned NOT NULL,
  `score` double(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `waves_heat_id_surfer_id_start_date_unique` (`heat_id`,`surfer_id`,`start_date`),
  KEY `waves_surfer_id_foreign` (`surfer_id`),
  CONSTRAINT `waves_heat_id_foreign` FOREIGN KEY (`heat_id`) REFERENCES `heats` (`id`) ON DELETE CASCADE,
  CONSTRAINT `waves_surfer_id_foreign` FOREIGN KEY (`surfer_id`) REFERENCES `surfers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `waves` (`id`, `start_date`, `finish_date`, `heat_id`, `surfer_id`, `score`)
VALUES
	(1,NULL,NULL,1,2,NULL);
