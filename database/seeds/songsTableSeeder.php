<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class songsTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('songs')->delete();

        $faker = Faker\Factory::create('es_ES');

        for($i=0;$i<10;$i++)
        {
            DB::table('songs')->insert(
                [
                    'title'     => $faker->sentence(),
                    'lyrics'    => $faker->paragraph(),
                    'slug'      => $faker->slug(),
                    'created_at'=> new DateTime,
                    'updated_at'=> new DateTime
                ]
            );
        }

    }

}
